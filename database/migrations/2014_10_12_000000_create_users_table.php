<?php

use Belebo\Models\{
    User\User, Location\Location
};
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(User::getTableName(), function (Blueprint $table) {
            $table->increments('id');
            $table->enum('gender', ['male', 'female']);
            $table->string('first_name');
            $table->string('last_name');
            $table->date('date_of_birth');
            $table->string('email')->unique();
            $table->string('phone');
            $table->string('address');
            $table->unsignedInteger('location_id');
            $table->foreign('location_id')->references('id')->on(Location::getTableName())
                ->onUpdate('no action')
                ->onDelete('no action');
            $table->enum('role', ['admin', 'client', 'provider']);
            $table->boolean('active')->default(0);
            $table->string('password')->nullable();
            $table->string('stripe_id')->nullable()->unique();
            $table->string('facebook_id')->nullable()->unique();
            $table->string('google_id')->nullable()->unique();
            $table->rememberToken();
            $table->timestamps();
        });
    }
}

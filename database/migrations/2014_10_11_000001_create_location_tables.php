<?php

use Belebo\Models\Location\{
    Department, City, Postcode, Location
};
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Department::getTableName(), function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('code', 3);
            $table->boolean('available')->default(false);
        });

        Schema::create(City::getTableName(), function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
        });

        Schema::create(Postcode::getTableName(), function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 5)->unique();
            $table->unsignedInteger('department_id');
            $table->foreign('department_id')->references('id')->on(Department::getTableName());
        });

        Schema::create(Location::getTableName(), function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('city_id');
            $table->foreign('city_id')->references('id')->on(City::getTableName());
            $table->unsignedInteger('postcode_id');
            $table->foreign('postcode_id')->references('id')->on(Postcode::getTableName());
            $table->float('lat', 10, 6);
            $table->float('lng', 10, 6);
            $table->point('point')->nullable();
            $table->unique(['city_id', 'postcode_id']);
        });

//        DB::statement("CREATE INDEX idx_location ON locations__city_postcode USING gist(point)");
    }
}

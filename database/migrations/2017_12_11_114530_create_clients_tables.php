    <?php

use Belebo\Models\User\{
    Client\Address, User
};
use Belebo\Models\Location\Location;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Address::getTableName(), function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on(User::getTableName())
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->string('name')->nullable();
            $table->enum('gender', ['male', 'female']);
            $table->string('first_name');
            $table->string('last_name');
            $table->string('phone');
            $table->string('address');
            $table->unsignedInteger('location_id');
            $table->foreign('location_id')->references('id')->on(Location::getTableName())
                ->onUpdate('no action')
                ->onDelete('no action');
            $table->float('lat', 10, 6);
            $table->float('lng', 10, 6);
            $table->boolean('default')->default(false);
            $table->timestamps();
        });
    }
}

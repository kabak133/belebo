<?php

use Belebo\Models\Question\{
    Category, Question
};
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Category::getTableName(), function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
        });

        Schema::create(Question::getTableName(), function (Blueprint $table) {
            $table->increments('id');
            $table->string('question')->unique();
            $table->text('answer');
            $table->unsignedInteger('category_id');
            $table->foreign('category_id')->references('id')->on(Category::getTableName())
                ->onDelete('cascade');
        });
    }
}

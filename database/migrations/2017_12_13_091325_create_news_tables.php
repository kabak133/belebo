<?php

use Belebo\Models\News\{
    News, Category, Tag
};
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Category::getTableName(), function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('slug')->unique();
        });

        Schema::create(News::getTableName(), function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('slug')->unique();
            $table->longText('text');
            $table->boolean('published')->default(0);
            $table->unsignedInteger('category_id');
            $table->foreign('category_id')->references('id')->on(Category::getTableName())
                ->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create(Tag::getTableName(), function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('slug')->unique();
        });

        Schema::create(str_singular(News::getTableName()) . '_tag', function (Blueprint $table) {
            $table->unsignedInteger('news_id');
            $table->foreign('news_id')->references('id')->on(News::getTableName())
                ->onDelete('cascade');
            $table->unsignedInteger('tag_id');
            $table->foreign('tag_id')->references('id')->on(Tag::getTableName())
                ->onDelete('cascade');
            $table->unique(['news_id', 'tag_id']);
        });
    }
}

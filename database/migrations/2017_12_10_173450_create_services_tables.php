<?php

use Belebo\Models\Service\{
    Service, Category
};
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Category::getTableName(), function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
        });

        Schema::create(Service::getTableName(), function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('price');
            $table->integer('duration');
            $table->text('details');
            $table->text('preparation');
            $table->text('trick');
            $table->unsignedInteger('category_id');
            $table->foreign('category_id')->references('id')->on(Category::getTableName())
                ->onDelete('cascade');
            $table->timestamps();
        });
    }
}

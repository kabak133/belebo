<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Belebo\Models\User\Provider\{
    Provider, Availability, Company, Representative, Shareholder
};
use Belebo\Models\User\User;
use Belebo\Models\Location\{
    Location, Department
};
use Belebo\Models\Service\Service;

class CreateProvidersTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Provider::getTableName(), function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on(User::getTableName())
                ->onDelete('cascade');
            $table->text('about')->nullable();
            $table->unsignedInteger('location_id');
            $table->foreign('location_id')->references('id')->on(Location::getTableName());
            $table->tinyInteger('radius')->default(10);
            $table->string('website')->nullable();
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->string('instagram')->nullable();
            $table->ipAddress('ip');
            $table->timestamps();
        });

        Schema::create(Company::getTableName(), function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('provider_id');
            $table->foreign('provider_id')->references('id')->on(Provider::getTableName())
                ->onDelete('cascade');
            $table->string('name');
            $table->string('siren', 9);
            $table->string('iban', 27);
            $table->string('address');
            $table->unsignedInteger('location_id');
            $table->foreign('location_id')->references('id')->on(Location::getTableName());
            $table->timestamps();
        });

        Schema::create(Representative::getTableName(), function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('provider_id');
            $table->foreign('provider_id')->references('id')->on(Provider::getTableName())
                ->onDelete('cascade');
            $table->enum('gender', ['male', 'female']);
            $table->string('first_name');
            $table->string('last_name');
            $table->date('date_of_birth');
            $table->string('address');
            $table->unsignedInteger('location_id');
            $table->foreign('location_id')->references('id')->on(Location::getTableName());
            $table->timestamps();
        });

        Schema::create(Shareholder::getTableName(), function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('provider_id');
            $table->foreign('provider_id')->references('id')->on(Provider::getTableName())
                ->onDelete('cascade');
            $table->enum('gender', ['male', 'female']);
            $table->string('first_name');
            $table->string('last_name');
            $table->date('date_of_birth');
            $table->string('address');
            $table->unsignedInteger('location_id');
            $table->foreign('location_id')->references('id')->on(Location::getTableName());
            $table->timestamps();
        });

        Schema::create(Availability::getTableName(), function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('provider_id');
            $table->foreign('provider_id')->references('id')->on(Provider::getTableName())
                ->onDelete('cascade');
            $table->boolean('unavailable')->default(false);
            $table->date('from')->nullable();
            $table->date('to')->nullable();
            $table->json('d1')->nullable();
            $table->json('d2')->nullable();
            $table->json('d3')->nullable();
            $table->json('d4')->nullable();
            $table->json('d5')->nullable();
            $table->json('d6')->nullable();
            $table->json('d0')->nullable();
            $table->timestamps();
        });

        Schema::create(str_singular(Provider::getTableName()) . '_department', function (Blueprint $table) {
            $table->unsignedInteger('provider_id');
            $table->foreign('provider_id')->references('id')->on(Provider::getTableName())
                ->onDelete('cascade');
            $table->unsignedInteger('department_id');
            $table->foreign('department_id')->references('id')->on(Department::getTableName())
                ->onDelete('cascade');
        });

        Schema::create(str_singular(Provider::getTableName()) . '_service', function (Blueprint $table) {
            $table->unsignedInteger('provider_id');
            $table->foreign('provider_id')->references('id')->on(Provider::getTableName())
                ->onDelete('cascade');
            $table->unsignedInteger('service_id');
            $table->foreign('service_id')->references('id')->on(Service::getTableName())
                ->onDelete('cascade');
        });
    }
}

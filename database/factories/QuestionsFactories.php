<?php

use Belebo\Models\Question\{
    Category, Question
};
use Faker\Generator as Faker;

$factory->define(Category::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->sentence,
    ];
});

$factory->define(Question::class, function (Faker $faker) {
    return [
        'question' => $faker->sentence,
        'answer' => $faker->sentence
    ];
});

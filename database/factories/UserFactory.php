<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->defineAs(Belebo\Models\User\User::class, 'baseUser', function (Faker $faker) {
    $gender = $faker->randomElement(['male', 'female']);
    return [
        'gender' => $gender,
        'first_name' => $faker->firstName($gender),
        'last_name' => $faker->lastName($gender),
        'address' => $faker->streetAddress,
        'location_id' => rand(1, 904)
    ];
});

$factory->defineAs(Belebo\Models\User\User::class, 'baseUserWithDOB', function (Faker $faker) use ($factory) {
    $user = $factory->raw(Belebo\Models\User\User::class, [], 'baseUser');
    return array_merge($user, [
        'date_of_birth' => $faker->dateTimeThisCentury->format('d/m/Y'),
    ]);
});

$factory->define(Belebo\Models\User\User::class, function (Faker $faker) use ($factory) {
    $user = $factory->raw(Belebo\Models\User\User::class, [], 'baseUserWithDOB');
    return array_merge($user, [
        'email' => $faker->email,
        'phone' => substr(preg_replace('/\D/', '', $faker->mobileNumber), -9),
        'password' => 'secret',
        'active' => $faker->boolean(80),
    ]);
});

$factory->defineAs(Belebo\Models\User\User::class, 'admin', function (Faker $faker) use ($factory) {
    $user = $factory->raw(Belebo\Models\User\User::class);
    return array_merge($user, ['role' => 'admin']);
});

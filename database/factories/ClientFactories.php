<?php

use Faker\Generator as Faker;
use Belebo\Models\Location\Location;

$factory->defineAs(Belebo\Models\User\User::class, 'client', function (Faker $faker) use ($factory) {
    $user = $factory->raw(Belebo\Models\User\User::class);
    return array_merge($user, ['role' => 'client']);
});

$factory->define(Belebo\Models\User\Client\Address::class, function (Faker $faker) use ($factory) {
    $location = Location::inRandomOrder()->first(['id', 'lat', 'lng']);
    $user = $factory->raw(Belebo\Models\User\User::class, [], 'baseUser');
    $user['location_id'] = $location->id;
    return array_merge($user, [
        'phone' => substr(preg_replace('/\D/', '', $faker->mobileNumber), -9),
        'name' => $faker->word,
        'lng' => $location->lng,
        'lat' => $location->lat
    ]);
});

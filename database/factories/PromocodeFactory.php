<?php

use Faker\Generator as Faker;

$factory->define(\Belebo\Models\Promocode\Promocode::class, function (Faker $faker){
    return [
        'name' => $faker->word,
        'discount' => rand(5, 10),
        'type_id' => rand(1,3)
    ];
});
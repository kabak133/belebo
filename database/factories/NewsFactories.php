<?php

use Belebo\Models\News\{
    News, Category, Tag
};
use Faker\Generator as Faker;

$factory->define(Category::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->word,
    ];
});

$factory->define(News::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'text' => $faker->text(4000),
        'published' => $faker->boolean,
    ];
});

$factory->define(Tag::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->word,
    ];
});

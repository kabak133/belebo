<?php

use Faker\Generator as Faker;
use Belebo\Models\User\User;
use Belebo\Models\User\Provider\{
    Availability, Provider, Company, Representative, Shareholder
};

$factory->defineAs(User::class, 'provider', function (Faker $faker) use ($factory) {
    $user = $factory->raw(User::class);
    return array_merge($user, ['role' => 'provider']);
});

$factory->define(Provider::class, function (Faker $faker) {
    return [
        'location_id' => rand(1, 904),
        'radius' => rand(1, 10) * 5,
        'ip' => $faker->ipv4
    ];
});

$factory->define(Company::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'siren' => preg_replace('/\D/', '', $faker->siren),
        'iban' => $faker->iban('fr'),
        'address' => $faker->streetAddress,
        'location_id' => rand(1, 904),
    ];
});

$factory->define(Representative::class, function (Faker $faker) use ($factory) {
    return $factory->raw(User::class, [], 'baseUserWithDOB');
});

$factory->define(Shareholder::class, function (Faker $faker) use ($factory) {
    return $factory->raw(Representative::class);
});

$factory->define(Availability::class, function (Faker $faker) {
    return [
        'd1' => [
            0 => '0' . rand(0, 9) . ':' . $faker->time('i'),
            1 => rand(10, 14) . ':' . $faker->time('i'),
            2 => rand(15, 18) . ':' . $faker->time('i'),
            3 => rand(19, 23) . ':' . $faker->time('i')
        ],
        'd2' => [
            0 => '0' . rand(0, 9) . ':' . $faker->time('i'),
            1 => rand(10, 14) . ':' . $faker->time('i'),
            2 => rand(15, 18) . ':' . $faker->time('i'),
            3 => rand(19, 23) . ':' . $faker->time('i')
        ],
        'd3' => [
            0 => '0' . rand(0, 9) . ':' . $faker->time('i'),
            1 => rand(10, 14) . ':' . $faker->time('i'),
            2 => rand(15, 18) . ':' . $faker->time('i'),
            3 => rand(19, 23) . ':' . $faker->time('i')
        ],
        'd4' => [
            0 => '0' . rand(0, 9) . ':' . $faker->time('i'),
            1 => rand(10, 14) . ':' . $faker->time('i'),
            2 => rand(15, 18) . ':' . $faker->time('i'),
            3 => rand(19, 23) . ':' . $faker->time('i')
        ],
        'd5' => [
            0 => '0' . rand(0, 9) . ':' . $faker->time('i'),
            1 => rand(10, 14) . ':' . $faker->time('i'),
            2 => rand(15, 18) . ':' . $faker->time('i'),
            3 => rand(19, 23) . ':' . $faker->time('i')
        ],
        'd6' => [
            0 => '0' . rand(0, 9) . ':' . $faker->time('i'),
            1 => rand(10, 14) . ':' . $faker->time('i'),
            2 => rand(15, 18) . ':' . $faker->time('i'),
            3 => rand(19, 23) . ':' . $faker->time('i')
        ],
        'd0' => [
            0 => '0' . rand(0, 9) . ':' . $faker->time('i'),
            1 => rand(10, 14) . ':' . $faker->time('i'),
            2 => rand(15, 18) . ':' . $faker->time('i'),
            3 => rand(19, 23) . ':' . $faker->time('i')
        ]
    ];
});

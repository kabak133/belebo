<?php

use Illuminate\Database\Seeder;
use Belebo\Models\Service\Service;

class ServicesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            ['name' => 'Femme'],
            ['name' => 'Homme'],
            ['name' => 'En duo'],
            ['name' => 'Sur mesure']
        ];

        $services = [
            ['name' => 'Brushing',
                'price' => '38', 'duration' => '45', 'category_id' => '1',
                'image' => 'brushing-femme.jpg',
                'details' =>
                    'La mise en forme sera réalisée par notre coiffeur après un diagnostic.
                    La prestation comprend le brushing',
                'preparation' => 'Vos cheveux doivent être lavés par vos soins le jour même du RDV',
                'trick' => 'Faire deux shampooings successifs et bien rincer. L’utilisation d’un shampooing professionnel est recommandé'],
            ['name' => 'Coiffage / attache',
                'price' => '55', 'duration' => '60', 'category_id' => '1',
                'image' => 'coiffage-femme.jpg',
                'details' =>
                    'Un coiffage tendance, une attache rapide des tresses. Laissez-vous tenter par les services de notre coiffeur qui mettra en valeur votre chevelure (prestation réalisée sur cheveux secs)
                    La prestation comprend la coiffure',
                'preparation' => 'Vos cheveux doivent être lavés par vos soins le jour même du RDV ou la veille',
                'trick' => 'Si vous avez des produits de styling notre coiffeur vous aidera à les utiliser afin de mettre en valeur votre chevelure'],
            ['name' => 'Mis en Forme (ondulation)', 'price' => '89', 'duration' => '120', 'category_id' => '1',
                'image' => 'mise-en-forme-femme.jpg',
                'details' =>
                    'Dynamisez votre chevelure grâce à une ondulation. Du mouvement le plus léger au plus tonique votre coiffeur donnera du ressort à vos envies.
                    La prestation comprend Shampooing + mis en forme + soin + séchage',
                'preparation' => 'Deux shampooings successifs sont à réaliser par vos soins juste avant la prestation',
                'trick' => 'Pour un résultat parfait et un meilleur finish l’utilisation de produits professionnels sont recommandés'],
            ['name' => 'Coupe', 'price' => '55', 'duration' => '60', 'category_id' => '1',
                'image' => 'coupe-femme.jpg',
                'details' =>
                    'Une envie de changer! Un moment de plaisir. Notre coiffeur après un diagnostic effectuera la structure de coupe qui mettra en valeur votre style.
                    La prestation comprend la coupe et le brushing',
                'preparation' => 'Vos cheveux doivent être lavés par vos soins le jour même du RDV',
                'trick' => 'Faire deux shampooings successifs et bien rincer. L’utilisation d’un produit professionnel est recommandé'],
            ['name' => 'Couleur', 'price' => '79', 'duration' => '90', 'category_id' => '1',
                'image' => 'couleur-femme.jpg',
                'details' =>
                    'Besoin de lumière, de fantaisie ou de folie. Illuminez votre chevelure grâce à notre coiffeur. De la repousse jusqu\'à la pointe de vos cheveux, laissez vous guider par les conseils de votre coloriste
                    La prestation comprend: Shampooing, coupe, couleur, soin et brushing',
                'preparation' => 'Le Diagnostic couleur et la prestation sont réalisés sur cheveux secs non lavés',
                'trick' => 'Faire la structure de coupe avant peut vous aider dans vos choix de couleurs et de placements.'],
            ['name' => 'Couleur + mèches', 'price' => '119', 'duration' => '120', 'category_id' => '1',
                'image' => 'couleur-mesches-femme.jpg',
                'details' =>
                    'Votre coloriste personnalisera votre couleur et votre balayage et fonction de votre structure de coupe.
                    La prestation comprend: Shampooing+couleur +mèches+ soin+ brushing',
                'preparation' => 'Technique réalisé sur cheveux secs',
                'trick' => 'Un service de mèches associé à la couleur permet de nuancer et de mettre en valeur vos cheveux. Créer du contraste en ayant plus de 3 tons d’écart entre la couleur et vos mèches.'],
            ['name' => 'Décoloration / Balayage', 'price' => '99', 'duration' => '120', 'category_id' => '1',
                'image' => 'decolor-femme.jpg',
                'details' =>
                    'Des blonds les plus froids aux blonds le plus chauds, des blonds les plus naturels aux blonds les plus sophistiqués. Votre coiffeur vous guidera dans votre choix.
                    La prestation comprends shampooing +décoloration (ou balayage)+brushing',
                'preparation' => 'Réalisation sur cheveux secs. Evitez de faire un Shampooing 48h avant un service de décoloration',
                'trick' => 'Pour une belle  harmonie entre la peau et les cheveux, des blonds froids pour une incarnation claire et les blonds chauds pour la typologie dorée.'],
            ['name' => 'Coupe', 'price' => '38', 'duration' => '45', 'category_id' => '2',
                'image' => 'coupe-homme.jpg',
                'details' =>
                    'Laissez vous conseiller par notre coiffeur expert en coupe masculine. Il sera dompter à la perfection votre chevelure.
                    La prestation comprend la coupe et le séchage',
                'preparation' => ' Vos cheveux doivent être lavés par vos soins le jour même du RDV',
                'trick' => ' L’utilisation de produit professionnel pour votre coiffage permet d’avoir un meilleur maintien, vos cheveux sont gainés et texturisés pour un finish impeccable.'],
            ['name' => 'Coupe + barbe', 'price' => '49', 'duration' => '60', 'category_id' => '2',
                'image' => 'coupe-barbe-homme.jpg',
                'details' =>
                    'Notre coiffeur effectuera la coupe et la taille de la barbe après un échange avec vous afin de déterminer vos attentes 
                    La prestation comprend la coupe, la taille de la barbe et le séchage',
                'preparation' => 'Vos cheveux doivent être lavés par vos soins le jour même du RDV',
                'trick' => 'L’utilisation d’un shampooing argent est conseillé une fois par semaine en cas de cheveux blancs pour éviter le jaunissement'],
            ['name' => 'Couleur / décoloration', 'price' => '99', 'duration' => '120', 'category_id' => '2',
                'image' => 'couleur-homme.jpg',
                'details' =>
                    'Vous pouvez estomper, camoufler vos cheveux blancs ou encore donnez du style à votre coupe en la personnalisant grâce à un éclaircissement. Osez passer le cap à l’aide des conseils de votre coiffeur.
                    La prestation comprend la coupe, la couleur ou décoloration et le séchage',
                'preparation' => 'Vos cheveux doivent être lavés par vos soins le jour même du RDV',
                'trick' => 'Un léger éclaircissement permet de donner une impression de volume, de relief et de matière.'],
            ['name' => 'Maman + enfant', 'price' => '84', 'duration' => '60', 'category_id' => '3',
                'image' => 'maman-enfant.jpg',
                'details' =>
                    'Gagnez du temps sur votre organisation. Faites vous chouchouter a’ la maison en famille
                    La prestation comprend:
                    Maman: Coupe Femme + Brushing
                    Enfant: Coupe Enfant + séchage',
                'preparation' => 'Vos Shampooings doivent être réalisés par vos soins le jour même du RDV',
                'trick' => 'Adapter votre structure de coupe la nature de vos cheveux'],
            ['name' => 'Coupe femme + homme', 'price' => '89', 'duration' => '60', 'category_id' => '3',
                'image' => 'coupe-femme-homme.jpg',
                'details' =>
                    'Pause détente en couple. On vous chouchoute, laissez vous tenter!
                    La prestation comprend
                    Femme: Coupe + Brushing
                    Homme: Coupe+ séchage',
                'preparation' => 'Vos Shampooings doivent être réalisés par vos soins le jour même du RDV',
                'trick' => 'Séchez vos cheveux après votre shampooing permet d’avoir plus de brillance et plus du mouvement.'],
            ['name' => 'Coupe homme + enfant', 'price' => '84', 'duration' => '60', 'category_id' => '3',
                'image' => 'papa-enfant.jpg',
                'details' =>
                    'Gagnez du temps sur votre organisation. Faites vous chouchouter à la maison en famille.
                    La prestation comprend:
                    Papa: Coupe Femme + séchage
                    Enfant: Coupe Enfant + séchage',
                'preparation' => 'Vos shampooings doivent être réalisés par vos soins le jour même du RDV',
                'trick' => 'Adapter votre structure de coupe la nature de vos cheveux'],
            ['name' => 'Mariage', 'price' => '0', 'duration' => '0', 'category_id' => '4',
                'image' => 'mariage.jpg',
                'details' => '',
                'preparation' => '',
                'trick' => ''],
            ['name' => 'Atelier', 'price' => '149', 'duration' => '120', 'category_id' => '4',
                'image' => 'ateliers.jpg',
                'details' => '',
                'preparation' => '',
                'trick' => ''],
        ];

        DB::table(\Belebo\Models\Service\Category::getTableName())->insert($categories);
        foreach ($services as $service) {
            $image = $service['image'];
            unset($service['image']);
            $srv = Service::create($service);
            $srv->addMedia(Storage::path('default/services/' . $image))->preservingOriginal()->toMediaCollection('image');
        }
    }
}

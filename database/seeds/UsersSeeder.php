<?php

use Belebo\Models\User\User;
use Belebo\Models\User\Client\Address;
use Belebo\Models\User\Provider\{
    Availability, Provider, Company, Representative, Shareholder
};
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::flushEventListeners();
        Provider::flushEventListeners();
        Address::flushEventListeners();
        Company::flushEventListeners();

        factory(User::class, 'client', 50)->create()
            ->each(function ($client) {
                $client->addresses()->saveMany(factory(Address::class, rand(0, 3))->make());
            });

        factory(User::class, 'provider', 50)->create()
            ->each(function ($user) {
                $provider = $user->provider()->save(factory(Provider::class)->make());
                $provider->company()->save(factory(Company::class)->make());
                $provider->representative()->save(factory(Representative::class)->make());
                $provider->shareholders()->saveMany(factory(Shareholder::class, rand(0, 4))->make());
                $provider->addMedia(Storage::path('default/horizontal.png'))->preservingOriginal()->toMediaCollection('passport');
                $provider->company->addMedia(Storage::path('default/horizontal.png'))->preservingOriginal()->toMediaCollection('certificate');
                $provider->representative->addMedia(Storage::path('default/horizontal.png'))->preservingOriginal()->toMediaCollection('passport');
                foreach ($provider->shareholders as $shareholder) {
                    $shareholder->addMedia(Storage::path('default/horizontal.png'))->preservingOriginal()->toMediaCollection('passport');
                }

                $services = range(1, 15);
                shuffle($services);
                $provider->services()->sync(array_slice($services, 0, rand(1, 15)));

                $provider->availability()->save(factory(Availability::class)->make());
            });

        factory(User::class, 'admin', 5)->create();
    }
}

<?php

use Illuminate\Database\Seeder;
use Belebo\Models\Promocode\PromocodeType;

class PromocodeTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
            'single use',
            'temporary',
            'referrer'
        ];

        foreach ($types as $type){
            PromocodeType::create(['name' => $type]);
        }
    }
}
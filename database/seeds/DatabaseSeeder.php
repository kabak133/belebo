<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Artisan::call('passport:install');
        $this->call(LocationsSeeder::class);
        $this->call(NewsCategoriesSeeder::class);
        $this->call(ServicesSeeder::class);
        $this->call(QuestionsSeeder::class);
        $this->call(ConditionsSeeder::class);
        $this->call(NewsSeeder::class);
        $this->call(UsersSeeder::class);
        $this->call(PromocodeTypeSeeder::class);
        $this->call(PromocodeSeeder::class);
    }
}

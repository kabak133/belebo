<?php

use Illuminate\Database\Seeder;
use Belebo\Models\News\{
    News, Category, Tag
};

class NewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Tag::class, 20)->create();
        $categories = Category::all();
        foreach ($categories as $category) {
            $category->news()->saveMany(factory(News::class, rand(5, 25))->make())->each(function ($news) {
                $tags = range(1, 20);
                shuffle($tags);
                $news->tags()->sync(array_slice($tags, 0, rand(0, 5)));
                $news->addMedia(Storage::path('default/horizontal.png'))->preservingOriginal()->toMediaCollection('horizontal');
                $news->addMedia(Storage::path('default/vertical.png'))->preservingOriginal()->toMediaCollection('vertical');
            });
        }
    }
}

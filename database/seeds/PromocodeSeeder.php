<?php
use Illuminate\Database\Seeder;
use Belebo\Models\Promocode\Promocode;

class PromocodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Promocode::class, 5)->create();
    }
}
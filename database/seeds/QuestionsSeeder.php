<?php

use Belebo\Models\Question\{
    Category, Question
};
use Illuminate\Database\Seeder;

class QuestionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'category' => 'Nos coiffeurs',
                'questions' => [
                    ['question' => 'Qui sont les coiffeurs?',
                        'answer' => 'Ce sont des professionnels de la coiffure. Ils sont indépendants et travaillent à domicile. Ils sont experts dans leur domaine.'],
                    ['question' => 'Comment les coiffeurs sont ils sélectionnés?',
                        'answer' => 'Les coiffeurs s’inscrivent sur notre site. Nous demandons un certains nombres de justificatifs et ensuite nous prenons contact avec eux. Si le profil du coiffeur correspond à nos attentes il rempli un formulaire et peu faire parti de notre équipe de professionnels. De plus une invitation à laisser un avis et commentaire vous sera envoyé par email  après la prestation, c est important pour nous pour évaluer nos prestataires'],
                    ['question' => 'Dois-je me laver les cheveux avant l’arrivée du coiffeur?',
                        'answer' => 'C’est en fonction du type de forfait que vous avez sélectionné. Voir les détails sur le site, en sélectionnant le forfait vous pouvez voir cette précision.'],
                    ['question' => 'Est ce que les coiffeurs ont tout le matériel nécessaire?',
                        'answer' => 'Oui bien sur, nos professionnels de la coiffure sont tous équipés du matériel nécessaire pour réaliser la prestation. Rien ne vous empêche de vous procurer un accessoire de votre choix si vous souhaitez agrémenter une attache ou autre. Les accessoires ne sont pas fournis.'],
                    ['question' => 'Puis je choisir le coiffeur?',
                        'answer' => 'Nous les avons choisis pour vous.<br>
                        C’est un service immédiat, ou sur rendez vous. C’est le coiffeur disponible le plus rapidement et celui qui correspond à votre demande qui se rendra sur le lieu de la prestation.'],
                    ['question' => 'Est-il possible de recommander le même coiffeur?',
                        'answer' => 'C’est possible que le même coiffeur revienne chez vous car c’est lui qui est disponible au moment de votre commande, mais vous ne pouvez pas choisir le même.'],
                    ['question' => 'Je fais des allergies, Est ce que je peux réserver une prestation à domicile?',
                        'answer' => 'Oui, bien sur, vous pouvez réserver une prestation à domicile.  Vous devez informer le prestataire de votre allergie en amont. Si vous êtes allergique à la coloration par exemple, le coiffeur fera le nécessaire pour ne pas vous mettre en contact avec les produits colorant.'],
                    ['question' => 'Quels produits utilisez-vous?',
                        'answer' => 'Chaque prestataire est libre d’utiliser les produits de son choix. Aucune marque n’est imposée.'],
                    ['question' => 'Est ce que le coiffeur peut voir mon avis?',
                        'answer' => ''],
                    ['question' => 'Est ce que je peux choisir un coiffeur homme ou femme?',
                        'answer' => 'Nous ne pouvons pas garantir si c’est un coiffeur homme ou une femme qui va intervenir pour la réalisation de la prestation. C’est la personne disponible et qui peut répondre à vos besoins qui réalisera la prestation.'],
                    ['question' => 'Combien de temps la prestation prend t-elle?',
                        'answer' => 'Un temps approximatif est noté sur les détails du forfait que vous choisirez. Ce temps peut varier en fonction de la longueur et de l’épaisseur de vos cheveux.'],
                ]
            ],
            [
                'category' => 'Zone géographique',
                'questions' => [
                    ['question' => 'Dans quelle ville vous déplacez-vous?',
                        'answer' => 'Nous nous déplaçons dans toutes les villes du Bas-Rhin et du Haut-Rhin'],
                    ['question' => 'Est-il possible de bénéficier de vos services exceptionnellement dans les départements limitrophes?',
                        'answer' => 'Oui, sur demande et acceptation d’un prestataire'],
                    ['question' => 'Est ce qu’il faut ajouter des frais de déplacements?',
                        'answer' => 'Non'],
                    ['question' => 'Ou dois-je recevoir le coiffeur, chez moi au bureau…?',
                        'answer' => 'Vous pouvez recevoir le coiffeur sur le lieu de votre choix a partir du moment ou celui ci permet de réaliser la prestation (lumière, prise électrique, eau)'],
                ]
            ],
            [
                'category' => 'Mariage',
                'questions' => [
                    ['question' => 'Que dois-je préparer pour le jour j, matériel, mes cheveux?',
                        'answer' => 'Toute ces formalités seront à voir avec la personne qui sera en charge du rendez vous, en fonction de vos besoins et de la prestation'],
                    ['question' => 'Est-il possible d’avoir un autre essai pour mon mariage?',
                        'answer' => 'Oui bien sur'],
                    ['question' => 'Combien de temps avant dois-je prévoir l’essai?',
                        'answer' => 'Idéalement, 1 mois avant c’est parfait pour la réalisation de votre essai.'],
                    ['question' => 'L’essai ne me convient pas, que dois-je faire?',
                        'answer' => 'Vous pouvez réserver un nouveau forfait attache mais il sera facturé car il ne rentre plus dans le cadre du forfait'],
                    ['question' => 'Est ce que je peux ajouter un invité?',
                        'answer' => 'Oui bien sur, dans la mesure ou le prestataire aura été prévenu afin qu’il disposer du temps nécessaire pour réaliser la prestation'],
                ]
            ],
            [
                'category' => 'Atelier',
                'questions' => [
                    ['question' => 'En quoi consiste l’atelier?',
                        'answer' => 'L’atelier consiste à vous donner des conseils des astuces et vous montrer des techniques simples et rapides de coiffages en tout genre. Nous pouvons faire cet atelier dans les meilleures conditions en accueillant jusqu’à 6 personnes pour une durée de deux heures maximum. Vous voulez surprendre vos amies, offrir un moment de détente dans la bonne humeur, n’hésitez pas a nous contacter.'],
                ]
            ],
            [
                'category' => 'Annulation',
                'questions' => [
                    ['question' => 'Comment annuler?',
                        'answer' => 'Vous pouvez via votre compte annuler ou modifier votre rendez vous. Une retenu sera faite sur une annulation moins de 24h avant ainsi que pour une réservation immédiate. Voir conditions générales d’utilisations'],
                    ['question' => 'Est ce que je peux changer ma réservation?',
                        'answer' => 'Vous ne pouvez pas changer mais annuler votre réservation sans frais jusqu’à 24h avant le rendez vous. Dans les 24h, l’intégralité du rendez vous sera facture. Une fois que vous avez reçu la confirmation d’annulation vous pouvez recommander une nouvelle prestation.'],
                    ['question' => 'Si j’annule, est ce que je suis remboursé?',
                        'answer' => 'Vous pouvez annuler votre réservation sans frais jusqu’à 24h avant le rendez vous. Dans les 24h, l’intégralité du rendez vous sera facture'],
                ]
            ],

            [
                'category' => 'Commander une prestation',
                'questions' => [
                    ['question' => 'Comment ça marche?',
                        'answer' => 'C’est très simple, vous sélectionnez le forfait qui vous convient vous payez en ligne. Nous recherchons le coiffeur disponible. Une confirmation par SMS vous sera transmise.  Vous êtes débitez une fois la prestation réalisée.'],
                    ['question' => 'Je n’arrive pas à commander?',
                        'answer' => 'Merci de nous en faire part par téléphone ou par email.'],
                    ['question' => 'Pouvez-vous garantir un service immédiat?',
                        'answer' => 'Le service immédiat sera honoré seulement si un coiffeur est disponible, sinon, nous vous invitons à renouveler votre demande ultérieurement ou prendre RDV.'],
                    ['question' => 'Quelle est la différence entre le service immédiat et le rendez-vous?',
                        'answer' => 'Le service immédiat vous permet de vous faire coiffer le jour j de votre envie en fonction évidemment des disponibilités de nos coiffeurs. Le RDV vous permet de vous organiser à l’avance et faire une réservation au moment ou vous le souhaitez depuis chez vous.'],
                    ['question' => 'Comment laisser un avis?',
                        'answer' => 'Vous recevrez par email suite a votre prestation, qui vous permettra de donner votre avis et noter un commentaire sur la prestation. Cela nous permettra d’améliorer la qualité de nos services.'],
                ]
            ],

            [
                'category' => 'Moyen de paiement',
                'questions' => [
                    ['question' => 'Le prix est il fixe?',
                        'answer' => 'Les prix sont fixes sur l’ensemble des forfaits, seul les forfaits sur mesure comme le mariage et l’atelier sont variable'],
                    ['question' => 'Quel moyen de paiement?',
                        'answer' => 'Toujours dans le but de simplifier les démarches, vous réglez par carte bancaire. Pour vous assurer des paiements sécurisés, nous avons décidé de travailler avec <a href="https://stripe.com/" target="_blank">Stripe</a>.'],
                    ['question' => 'Est-il possible d’offrir un bon cadeau?',
                        'answer' => 'Nous vous offrons la possibilité de réserver le forfait que vous souhaitez et de l’offrir à la personne de votre choix. Vous commandez en ligne et la personne recevra un email avec un code qu’elle utilisera dans un délai de 3 mois maximum.'],
                    ['question' => 'Y a-t-il des réductions pour l’achat de plusieurs forfaits?',
                        'answer' => 'Oui vous obtiendrai des remises progressives en fonction du nombre de forfaits que vous réservez'],
                    ['question' => 'Est-ce que je peux recevoir une facture?',
                        'answer' => 'Votre facture sera disponible sur votre compte dans un délai de 14 jours'],
                ]
            ],

            [
                'category' => 'Parrainage',
                'questions' => [
                    ['question' => 'Est-il possible de parrainer une amie?',
                        'answer' => 'Vous pouvez tout a fait parrainer une ou plusieurs amies qui recevrons un code promotionnel à utiliser dans les trois mois. Vous recevrez en tant que parrain, un code promotionnel à valoir également dans les trois mois.'],
                ]
            ],
        ];
        foreach ($categories as $category) {
            $cat = Category::create(['name' => $category['category']])->id;
            foreach ($category['questions'] as $question) {
                Question::create(array_merge($question, ['category_id' => $cat]));
            }
        }
    }
}

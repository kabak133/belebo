<?php

use Illuminate\Database\Seeder;

class NewsCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            ['name' => 'Conseils'],
            ['name' => 'Tendance'],
            ['name' => 'Le saviez vous?'],
            ['name' => 'Le chouchou du mois!']
        ];

        foreach ($categories as $category) {
            \Belebo\Models\News\Category::create($category);
        }
    }
}

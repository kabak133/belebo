<?php

namespace Belebo\Providers;

use Belebo\Models\Location\{
    City, Postcode, Location
};
use Belebo\Observers\{
    LocationCityObserver, LocationPostcodeObserver, LocationObserver
};
use Belebo\Models\User\User;
use Belebo\Observers\UserObserver;
use Belebo\Models\User\Client\Address;
use Belebo\Observers\ClientAddressObserver;
use Belebo\Models\User\Provider\Company;
use Belebo\Observers\ProviderCompanyObserver;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        City::observe(LocationCityObserver::class);
        Postcode::observe(LocationPostcodeObserver::class);
        Location::observe(LocationObserver::class);
        User::observe(UserObserver::class);
        Address::observe(ClientAddressObserver::class);
        Company::observe(ProviderCompanyObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

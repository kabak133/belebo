<?php

namespace Belebo\Providers;

use Belebo\Helpers;
use Illuminate\Support\ServiceProvider;

class HelperServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the additional application helpers.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register the additional application helpers.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('date_helper', Helpers\DateHelper::class);
        $this->app->bind('file_helper', Helpers\FileHelper::class);
        $this->app->bind('stripe_helper', Helpers\StripeHelper::class);
    }
}

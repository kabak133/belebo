<?php

namespace Belebo\Rules;

use Belebo\Models\Location\Location;
use Illuminate\Contracts\Validation\Rule;

class UniqueLocation implements Rule
{
    private $id;
    private $city;
    private $postcode;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($city = null, $postcode = null, $id = null)
    {
        $this->id = $id;
        $this->city = $city;
        $this->postcode = $postcode;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ($this->city) {
            $this->postcode = $value;
        } else {
            $this->city = $value;
        }
        $location = Location::wherePostcodeId($this->postcode)->whereCityId($this->city);
        if ($this->id) {
            $location->whereKeyNot($this->id);
        }
        return $location->get()->isEmpty();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Location already exists';
    }
}

<?php

namespace Belebo\Observers;

use Geocoder;
use Belebo\Models\Location\Location;

class LocationObserver
{
    /**
     * Action on location creation
     *
     * @param Location $location
     */
    public function creating(Location $location)
    {
        $this->updateCoordinates($location);
    }

    /**
     * Action on location updating
     *
     * @param Location $location
     */
    public function updating(Location $location)
    {
        $this->updateCoordinates($location);
    }

    /**
     * Update location coordinates
     *
     * @param Location $location
     */
    private function updateCoordinates(Location $location)
    {
        if ($location->isDirty('city_id') || $location->isDirty('postcode_id')) {
            $coordinates = Geocoder::getCoordinatesForAddress($location->postcode . ' ' . $location->city);
            $location->lat = $coordinates['lat'];
            $location->lng = $coordinates['lng'];
        }
    }
}

<?php

namespace Belebo\Observers;

use Geocoder;
use Belebo\Models\Location\City;

class LocationCityObserver
{
    /**
     * Action on city updating
     *
     * @param City $city
     */
    public function updating(City $city)
    {
        $locations = $city->locations()->with('postcode')->get();
        foreach ($locations as $location) {
            $coordinates = Geocoder::getCoordinatesForAddress($location->postcode->code . ' ' . $city->name);
            $location->lat = $coordinates['lat'];
            $location->lng = $coordinates['lng'];
            $location->save();
        }
    }
}

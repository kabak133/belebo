<?php

namespace Belebo\Observers;

use Geocoder;
use Belebo\Models\Location\Location;
use Belebo\Models\User\Client\Address;

class ClientAddressObserver
{
    public function saving(Address $address)
    {
        $this->setCoordinates($address);
        $this->manageDefault($address);
    }

    /**
     * Set coordinates if not settled on address or location_id change
     *
     * @param Address $address
     */
    private function setCoordinates(Address $address)
    {
        if ($address->isDirty('address', 'location_id') && !$address->isDirty('lat', 'lng')) {
            $location = Location::find($address->location_id);
            $coordinates = Geocoder::getCoordinatesForAddress($address->address . ', ' . $location->name);
            if ($coordinates['formatted_address'] != 'result_not_found') {
                $address->lat = $coordinates['lat'];
                $address->lng = $coordinates['lng'];
            } else {
                $address->lat = $location['lat'];
                $address->lng = $location['lng'];
            }
        }
    }

    /**
     * Remove default address mark if current one became default
     *
     * @param $address
     */
    private function manageDefault(Address $address)
    {
        if ($address->default && !$address->getOriginal('default')) {
            Address::where('user_id', $address->user_id)->update(['default' => false]);
        }
    }
}

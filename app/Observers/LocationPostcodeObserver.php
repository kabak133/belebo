<?php

namespace Belebo\Observers;

use Geocoder;
use Belebo\Models\Location\Postcode;

class LocationPostcodeObserver
{
    /**
     * Action on postcode updating
     *
     * @param Postcode $postcode
     */
    public function updating(Postcode $postcode)
    {
        $locations = $postcode->locations()->with('city')->get();
        foreach ($locations as $location) {
            $coordinates = Geocoder::getCoordinatesForAddress($postcode->code . ' ' . $location->city->name);
            $location->lat = $coordinates['lat'];
            $location->lng = $coordinates['lng'];
            $location->save();
        }
    }
}

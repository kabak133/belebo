<?php

namespace Belebo\Observers;

use Stripe\Account;
use StripeHelper;
use Stripe\Customer;
use Belebo\Models\User\User;
use Belebo\Notifications\UserActivation;
use Belebo\Notifications\UserDeactivation;

class UserObserver
{
    /**
     * Action on user creation
     *
     * @param User $user
     */
    public function creating(User $user)
    {
        if ($user->isClient) {
            $user->stripe_id = StripeHelper::createCustomer($user);
        }
    }

    /**
     * * Action on user updating
     *
     * @param User $user
     */
    public function updating(User $user)
    {

        $password = null;
        if ($user->isClient && $user->isDirty('email')) {
            $stripe_usr = Customer::retrieve($user->stripe_id);
            $stripe_usr->email = $user->email;
            $stripe_usr->save();
        } elseif ($user->isProvider) {
            if ($user->active && !$user->hasStripe) {
                $user->stripe_id = StripeHelper::createAccount($user);
                $user->password = $password = str_random(8);
            } elseif ($user->hasStripe && $user->isDirty('email')) {
                $stripe_usr = Account::retrieve($user->stripe_id);
                $stripe_usr->email = $user->email;
                $stripe_usr->save();
            }
        }

        if ($user->active != $user->getOriginal('active')) {
            $user->notify($user->active ? new UserActivation($password) : new UserDeactivation());
        }
    }

    /**
     * Action after user destroyed
     *
     * @param User $user
     */
    public function deleted(User $user)
    {
        if ($user->stripe_id) {
            if ($user->isClient) {
                $customer = Customer::retrieve($user->stripe_id);
                $customer->delete();
            } elseif ($user->isProvider) {
                $acc = Account::retrieve($user->stripe_id);
                $acc->delete();
            }
        }
    }
}

<?php

namespace Belebo\Observers;

use Stripe\Account;
use StripeHelper;
use Belebo\Models\User\Provider\Company;

class ProviderCompanyObserver
{
    /**
     * * Action on user updating
     *
     * @param Company $company
     */
    public function updating(Company $company)
    {
        $id = $company->provider->user->stripe_id;
        if ($id) {
            $acc = Account::retrieve($id);
            if ($company->isDirty('name')) {
                $acc->legal_entity->business_name = $company->name;
                $acc->business_name = $company->name;
            }
            if ($company->isDirty('iban')) {
                $acc->external_account = StripeHelper::createIbanToken($company->iban);
            }
            if ($company->isDirty('siren')) {
                $acc->legal_entity->business_tax_id = $company->siren;
            }
            if ($company->isDirty('address')) {
                $acc->legal_entity->address->line1 = $company->address;
            }
            if ($company->isDirty('location_id')) {
                $acc->legal_entity->address->city = $company->city;
                $acc->legal_entity->address->postal_code = $company->postcode;
            }
            $acc->save();
        }
    }
}

<?php

namespace Belebo\Helpers;

use Carbon\Carbon;

class DateHelper
{
    /**
     * Check is date valid and return Carbon object or null if invalid
     *
     * @param string $date
     * @param string $format
     * @return Carbon | null
     */
    function createDate($date, $format = 'd/m/Y')
    {
        $d = false;
        if (!is_null($date)) {
            $d = Carbon::createFromFormat($format, $date);
        }
        if ($d && $d->format($format) == $date) {
            return $d;
        } else {
            return null;
        }
    }
}

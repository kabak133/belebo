<?php

namespace Belebo\Helpers;

use Carbon\Carbon;

class FileHelper
{
    /**
     * Convert file to base64 using path
     *
     * @param string $path
     * @return string
     */
    function toBase64($path)
    {
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);
        return 'data:image/' . $type . ';base64,' . base64_encode($data);
    }
}

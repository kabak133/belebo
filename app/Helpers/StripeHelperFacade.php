<?php

namespace Belebo\Helpers;

use Illuminate\Support\Facades\Facade;

class StripeHelperFacade extends Facade
{
    protected static function getFacadeAccessor() { return 'stripe_helper'; }
}

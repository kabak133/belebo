<?php

namespace Belebo\Helpers;

use Illuminate\Support\Facades\Facade;

class DateHelperFacade extends Facade
{
    protected static function getFacadeAccessor() { return 'date_helper'; }
}

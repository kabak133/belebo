<?php

namespace Belebo\Helpers;

use Request;
use Stripe\Token;
use Stripe\Account;
use Stripe\Customer;
use Stripe\FileUpload;
use Belebo\Models\User\User;
use Belebo\Models\User\Provider\BaseRepresentative;

class StripeHelper
{
    /**
     * Create stripe IBAN token
     *
     * @param string $iban
     * @return mixed|null
     */
    public function createIbanToken(string $iban)
    {
        return Token::create([
            'bank_account' => [
                'country' => 'FR',
                'currency' => 'eur',
                'account_number' => $iban,
            ],
        ])->id;
    }

    public function uploadPassport(BaseRepresentative $representative)
    {
        return FileUpload::create([
            'purpose' => 'identity_document',
            'file' => fopen($representative->getFirstMedia('passport')->getPath(), 'r')
        ])->id;
    }

    public function createAccount(User $user) {
        $acc = [
            'type' => 'custom',
            'country' => 'FR',
            'email' => $user->email,
            'business_name' => $user->provider->company->name,
            'external_account' => StripeHelper::createIbanToken($user->provider->company->iban),
            'legal_entity' => [
                'business_name' => $user->provider->company->name,
                'business_tax_id' => $user->provider->company->siren,
                'type' => 'company',
                'address' => [
                    'line1' => $user->provider->company->address,
                    'postal_code' => $user->provider->company->postcode,
                    'city' => $user->provider->company->city,
                ],
                'first_name' => $user->provider->representative->first_name,
                'last_name' => $user->provider->representative->last_name,
                'dob' => [
                    'day' => $user->provider->representative->date_of_birth->day,
                    'month' => $user->provider->representative->date_of_birth->month,
                    'year' => $user->provider->representative->date_of_birth->year,
                ],
                'personal_address' => [
                    'line1' => $user->provider->representative->address,
                    'postal_code' => $user->provider->representative->postcode,
                    'city' => $user->provider->representative->city,
                ],
                'verification' => ['document' => StripeHelper::uploadPassport($user->provider->representative)]
            ],
            'tos_acceptance' => [
                'ip' => Request::ip(),
                'date' => $user->created_at->timestamp,
            ]
        ];

        if ($user->provider->shareholders->isNotEmpty()) {
            foreach ($user->provider->shareholders as $shareholder) {
                $acc['legal_entity']['additional_owners'][] = [
                    'first_name' => $shareholder->first_name,
                    'last_name' => $shareholder->last_name,
                    'dob' => [
                        'day' => $shareholder->date_of_birth->day,
                        'month' => $shareholder->date_of_birth->month,
                        'year' => $shareholder->date_of_birth->year,
                    ],
                    'address' => [
                        'line1' => $shareholder->address,
                        'postal_code' => $shareholder->postcode,
                        'city' => $shareholder->city,
                    ],
                    'verification' => ['document' => StripeHelper::uploadPassport($shareholder)]
                ];
            }
        } else {
            $acc['legal_entity']['additional_owners'] = null;
        }
        return Account::create($acc)->id;
    }

    public function deleteAccount(User $user) {
        $acc = Account::retrieve($user->stripe_id);
        $acc->delete();
    }

    public function createCustomer(User $user) {
        return Customer::create([
            'description' => $user->name,
            'email' => $user->email
        ])->id;
    }
}

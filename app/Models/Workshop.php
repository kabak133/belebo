<?php

namespace Belebo\Models;

use Carbon\Carbon;
use Belebo\Traits\TableNameGetter;
use Illuminate\Database\Eloquent\Model;

class Workshop extends Model
{
    use TableNameGetter;

    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'phone',
        'datetime',
        'participants',
        'address',
        'message'
    ];

    protected $dates = ['datetime'];

    /**
     * Transform the datetime string to Carbon object
     *
     * @param string $value
     */
    public function setDatetimeAttribute($value)
    {
        $this->attributes['datetime'] = Carbon::createFromFormat('d/m/Y H:i', $value);
    }
}

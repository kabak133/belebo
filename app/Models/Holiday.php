<?php

namespace Belebo\Models;

use Illuminate\Database\Eloquent\Model;

class Holiday extends Model
{
    protected $fillable = [
        'name', 'date'
    ];

    public $timestamps  = false;


}

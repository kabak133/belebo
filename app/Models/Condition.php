<?php

namespace Belebo\Models;

use Belebo\Traits\TableNameGetter;
use Illuminate\Database\Eloquent\Model;

class Condition extends Model
{
    use TableNameGetter;

    protected $fillable = ['type', 'text'];

    public $timestamps = false;

    public function getRouteKeyName()
    {
        return 'type';
    }
}

<?php

namespace Belebo\Models\Service;

use Belebo\Models\Order;
use Belebo\Traits\TableNameGetter;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Service extends Model implements HasMedia
{
    use HasMediaTrait, TableNameGetter;

    protected $table = 'services__services';

    protected $fillable = [
        'name',
        'price',
        'duration',
        'category_id',
        'details',
        'preparation',
        'trick',
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function orders()
    {
        return $this->belongsToMany(
            Order::class, 'orders_services',
            'service_id', 'order_id');
    }

    public function registerMediaCollections()
    {
        $this
            ->addMediaCollection('image')
            ->singleFile();
    }
}

<?php

namespace Belebo\Models\Service;

use Belebo\Traits\TableNameGetter;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use TableNameGetter;

    protected $table = 'services__categories';

    protected $fillable = ['name'];

    public $timestamps = false;

    public function services()
    {
        return $this->hasMany(Service::class);
    }
}

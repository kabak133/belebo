<?php

namespace Belebo\Models\Question;

use Belebo\Traits\TableNameGetter;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    use TableNameGetter;

    protected $table = 'questions__questions';

    protected $fillable = ['question', 'answer', 'category_id'];

    public $timestamps = false;

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}

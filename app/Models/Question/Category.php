<?php

namespace Belebo\Models\Question;

use Belebo\Traits\TableNameGetter;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use TableNameGetter;

    protected $table = 'questions__categories';

    protected $fillable = ['name'];

    public $timestamps = false;

    public function questions()
    {
        return $this->hasMany(Question::class);
    }
}

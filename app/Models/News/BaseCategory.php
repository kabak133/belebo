<?php

namespace Belebo\Models\News;

use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Database\Eloquent\Model;

abstract class BaseCategory extends Model
{
    use HasSlug;

    protected $fillable = [
        'name',
        'slug'
    ];

    public $timestamps = false;

    public function news()
    {
        return $this->hasMany(News::class);
    }

    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }
}

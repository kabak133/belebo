<?php

namespace Belebo\Models\News;

use DateHelper;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Belebo\Traits\TableNameGetter;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class News extends Model implements HasMedia
{
    use HasMediaTrait, HasSlug, TableNameGetter;

    protected $table = 'news__news';

    protected $fillable = [
        'title',
        'slug',
        'text',
        'published',
        'category_id'
    ];

    protected $with = ['tags'];

    public function scopePublished($query)
    {
        $query->wherePublished(1);
    }

    public function scopeFindPublishedBySlug($query, $slug)
    {
        return $query->with('category')->whereSlug($slug)->published()->firstOrFail();
    }

    public function scopeFilteredByDate($query, $date)
    {
        $date = DateHelper::createDate($date, 'm-Y');
        if ($date) {
            $query->whereMonth('created_at', $date->format('m'))->whereYear('created_at', $date->format('Y'));
        }
        return $query->latest();
    }

    public function scopeFindPublishedByCategory($query, $category)
    {
        return $query->whereHas('category', function ($query) use ($category) {
            $query->where('slug', $category);
        })->published()->latest();
    }

    public function scopeFindPublishedByTag($query, $tag)
    {
        return $query->whereHas('tags', function ($query) use ($tag) {
            $query->whereSlug($tag);
        })->published()->latest();
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, str_singular(self::getTableName()) . '_tag');
    }

    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug');
    }

    public function registerMediaCollections()
    {
        $this
            ->addMediaCollection('horizontal')
            ->singleFile()
            ->registerMediaConversions(function () {
                $this
                    ->addMediaConversion('thumb-lg')
                    ->width(600);
                $this
                    ->addMediaConversion('thumb-sm')
                    ->width(300);
            });
        $this
            ->addMediaCollection('vertical')
            ->singleFile()
            ->registerMediaConversions(function () {
                $this
                    ->addMediaConversion('thumb')
                    ->width(150);
            });
    }
}

<?php

namespace Belebo\Models\News;

use Belebo\Traits\TableNameGetter;

class Category extends BaseCategory
{
    use TableNameGetter;

    protected $table = 'news__categories';
}

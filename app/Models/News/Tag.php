<?php

namespace Belebo\Models\News;

use Belebo\Traits\TableNameGetter;

class Tag extends BaseCategory
{
    use TableNameGetter;

    protected $table = 'news__tags';

    public function news()
    {
        return $this->belongsToMany(News::class, str_singular(News::getTableName()) . '_tag');
    }
}

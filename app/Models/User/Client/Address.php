<?php

namespace Belebo\Models\User\Client;

use Belebo\Traits\HasLocation;
use Belebo\Traits\TableNameGetter;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    use HasLocation, TableNameGetter;

    protected $table = 'users__client_addresses';

    protected $fillable = [
        'user_id',
        'name',
        'gender',
        'first_name',
        'last_name',
        'phone',
        'address',
        'location_id',
        'lat',
        'lng',
        'default'
    ];
}

<?php

namespace Belebo\Models\User;

use Hash;
use Crypt;
use Carbon\Carbon;
use Belebo\Traits\HasLocation;
use Belebo\Traits\TableNameGetter;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, HasLocation, Notifiable, TableNameGetter;

    /**
     * Users table name
     *
     * @var string
     */
    protected $table = 'users__users';

    protected $casts = [
        'active' => 'boolean'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'gender',
        'first_name',
        'last_name',
        'date_of_birth',
        'email',
        'phone',
        'address',
        'location_id',
        'role',
        'active',
        'password',
        'stripe_id',
        'facebook_id',
        'google_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'facebook_id', 'google_id', 'stripe_id'
    ];

    /**
     * The attributes that should be transformed to Carbon object.
     *
     * @var array
     */
    protected $dates = ['date_of_birth'];

    /**
     * Get full name
     *
     * @return string
     */
    public function getNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * Get phone number with code
     *
     * @return string
     */
    public function getPhoneNumberAttribute()
    {
        return '+33'.$this->phone;
    }
    /**
     * Transform the date string to Carbon object
     *
     * @param string $value
     */
    public function setDateOfBirthAttribute($value)
    {
        $this->attributes['date_of_birth'] = Carbon::createFromFormat('d/m/Y', $value);
    }

    /**
     * Make hash from a plain password if exists
     *
     * @param $value
     */
    public function setPasswordAttribute($value)
    {
        if (is_null($value)) {
            $this->attributes['password'] = null;
        } else {
            $this->attributes['password'] = Hash::make($value);
        }
    }

    /**
     * Checking whether the user has a password
     *
     * @return bool
     */
    public function getHasPasswordAttribute()
    {
        return boolval($this->password);
    }

    /**
     * Checking whether the user has a password
     *
     * @return bool
     */
    public function getHasStripeAttribute()
    {
        return boolval($this->stripe_id);
    }

    /**
     * Checking whether the user is an admin
     *
     * @return bool
     */
    public function getIsAdminAttribute()
    {
        return $this->role == 'admin';
    }

    /**
     * Checking whether the user is a client
     *
     * @return bool
     */
    public function getIsClientAttribute()
    {
        return $this->role == 'client';
    }

    /**
     * Checking whether the user is a provider
     *
     * @return bool
     */
    public function getIsProviderAttribute()
    {
        return $this->role == 'provider';
    }

    /**
     * Find user by share token
     *
     * @param $token
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    public function scopeFindByToken($query, $token)
    {
        $user = $this->with('provider')->findOrFail(Crypt::decrypt($token));
        if ($user->isProvider && $user->active) {
            return $user;
        } else {
            abort(404);
        }
    }

    /**
     * Get customers
     * scopeClients returns an error that method should not be call statically, so scopeCustomers used
     *
     * @param $query
     * @return mixed
     */
    public function scopeCustomers($query)
    {
        return $query->where('role', 'client');
    }

    /**
     * Get providers
     *
     * @param $query
     * @return mixed
     */
    public function scopeProviders($query)
    {
        return $query->where('role', 'provider');
    }

    /**
     * Get admins
     *
     * @param $query
     * @return mixed
     */
    public function scopeAdmins($query)
    {
        return $query->where('role', 'admin');
    }

    /**
     * Get client addresses
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function addresses()
    {
        return $this->hasMany(Client\Address::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function provider()
    {
        return $this->hasOne(Provider\Provider::class);
    }
}

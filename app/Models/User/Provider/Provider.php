<?php

namespace Belebo\Models\User\Provider;

use Belebo\Models\Service\Service;
use Belebo\Models\User\User;
use Belebo\Traits\HasLocation;
use Belebo\Traits\TableNameGetter;
use Belebo\Models\Location\Department;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Provider extends Model implements HasMedia
{
    use HasLocation, HasMediaTrait, TableNameGetter;

    protected $table = 'users__providers';

    protected $fillable = [
        'user_id',
        'stripe_id',
        'ip',
        'about',
        'location_id',
        'twitter',
        'instagram',
        'facebook',
        'website',
        'radius'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get provider company data
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function company()
    {
        return $this->hasOne(Company::class);
    }

    /**
     * Get provider company representative data
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function representative()
    {
        return $this->hasOne(Representative::class);
    }

    /**
     * Get provider company shareholders
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function shareholders()
    {
        return $this->hasMany(Shareholder::class);
    }

    /**
     * Get provider services
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function services()
    {
        return $this->belongsToMany(Service::class, str_singular(Provider::getTableName()) . '_service');
    }

    /**
     * Get provider availability
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function availability()
    {
        return $this->hasOne(Availability::class);
    }

    /**
     * Get chosen departments
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function departments()
    {
        return $this->belongsToMany(Department::class, str_singular(self::getTableName()) . '_department');
    }

    public function registerMediaCollections()
    {
        $this
            ->addMediaCollection('passport')
            ->useDisk('documents')
            ->singleFile();
        $this
            ->addMediaCollection('photo')
            ->singleFile();
        /*
         ->registerMediaConversions(function () {
            $this
                ->addMediaConversion('thumb')
                ->width(150);
        })
        */
    }
}

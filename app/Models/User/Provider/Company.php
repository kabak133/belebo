<?php

namespace Belebo\Models\User\Provider;

use Belebo\Traits\HasLocation;
use Belebo\Traits\TableNameGetter;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Company extends Model implements HasMedia
{
    use HasLocation, HasMediaTrait, TableNameGetter;

    protected $table = 'users__providers__companies';

    protected $fillable=[
        'provider_id',
        'name',
        'siren',
        'iban',
        'address',
        'location_id',
    ];

    public function provider() {
        return $this->belongsTo(Provider::class);
    }

    public function registerMediaCollections()
    {
        $this
            ->addMediaCollection('certificate')
            ->useDisk('documents')
            ->singleFile();
    }
}

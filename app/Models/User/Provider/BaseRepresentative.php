<?php

namespace Belebo\Models\User\Provider;

use Carbon\Carbon;
use Belebo\Traits\HasLocation;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

abstract class BaseRepresentative extends Model implements HasMedia
{
    use HasLocation, HasMediaTrait;

    protected $fillable = [
        'provider_id',
        'gender',
        'first_name',
        'last_name',
        'date_of_birth',
        'address',
        'location_id',
    ];

    protected $dates = ['date_of_birth'];

    /**
     * Transform the date string to Carbon object
     *
     * @param string $value
     */
    public function setDateOfBirthAttribute($value)
    {
        $this->attributes['date_of_birth'] = Carbon::createFromFormat('d/m/Y', $value);
    }

    public function registerMediaCollections()
    {
        $this
            ->addMediaCollection('passport')
            ->singleFile();
    }
}

<?php

namespace Belebo\Models\User\Provider;

use Belebo\Traits\TableNameGetter;

class Shareholder extends BaseRepresentative
{
    use TableNameGetter;

    protected $table = 'users__providers__shareholders';
}

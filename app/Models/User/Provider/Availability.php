<?php

namespace Belebo\Models\User\Provider;

use Carbon\Carbon;
use Belebo\Traits\TableNameGetter;
use Illuminate\Database\Eloquent\Model;

class Availability extends Model
{
    use TableNameGetter;

    protected $table = 'users__providers__availability';

    protected $casts = [
        'd1' => 'array',
        'd2' => 'array',
        'd3' => 'array',
        'd4' => 'array',
        'd5' => 'array',
        'd6' => 'array',
        'd0' => 'array'
    ];

    protected $fillable = [
        'provider_id',
        'd1', 'd2', 'd3', 'd4', 'd5', 'd6', 'd0',
        'unavailable',
        'from',
        'to'
    ];

    protected $dates = ['from', 'to'];

    public function getFormattedFromAttribute()
    {
        if ($this->from) {
            return $this->from->format('d/m/Y');
        } else {
            return null;
        }
    }

    public function getFormattedToAttribute()
    {
        if ($this->to) {
            return $this->to->format('d/m/Y');
        } else {
            return null;
        }
    }

    public function setFromAttribute($value)
    {
        if ($value) {
            $this->attributes['from'] = Carbon::createFromFormat('d/m/Y', $value);
        }
    }

    public function setToAttribute($value)
    {
        if ($value) {
            $this->attributes['to'] = Carbon::createFromFormat('d/m/Y', $value);
        }
    }
}

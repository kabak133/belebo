<?php

namespace Belebo\Models\User\Provider;

use Belebo\Traits\TableNameGetter;

class Representative extends BaseRepresentative
{
    use TableNameGetter;

    protected $table = 'users__providers__representatives';
}

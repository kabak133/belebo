<?php

namespace Belebo\Models\Location;

use Belebo\Traits\TableNameGetter;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    use TableNameGetter;

    protected $table = 'locations__departments';

    protected $fillable = [
        'name',
        'code',
        'available'
    ];

    public $timestamps = false;

    public function postcodes()
    {
        return $this->hasMany(Postcode::class);
    }

    public function scopeAvailable($query)
    {
        return $query->where('available', true);
    }

    public function scopeUnavailable($query)
    {
        return $query->where('available', false);
    }
}

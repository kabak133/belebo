<?php

namespace Belebo\Models\Location;

use Belebo\Traits\TableNameGetter;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    use TableNameGetter;

    protected $table = 'locations__city_postcode';

    protected $fillable = ['city_id', 'postcode_id', 'lat', 'lng', 'point'];

    public $timestamps = false;

    protected $postgisFields = [
        'point'
    ];

    protected $postgisTypes = [
        'point' => [
            'geomtype' => 'geography',
            'srid' => 4326
        ]
        ];

    protected $with = ['city', 'postcode'];

    public function getNameAttribute()
    {
        return $this->postcode->code . ' ' . $this->city->name;
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function postcode()
    {
        return $this->belongsTo(Postcode::class);
    }

    public function scopeFindByCity($query, $city)
    {
        return $query->whereHas('city', function ($query) use ($city) {
            $query->where('name', 'like', '%' . $city . '%');
        });
    }

    public function scopeFindByCode($query, $code)
    {
        return $query->whereHas('postcode', function ($query) use ($code) {
            $query->where('code', 'like', '%' . $code . '%');
        });
    }
}

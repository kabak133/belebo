<?php

namespace Belebo\Models\Location;

use Belebo\Traits\TableNameGetter;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    use TableNameGetter;

    protected $table = 'locations__cities';

    protected $fillable = ['name'];

    public $timestamps = false;

    public function postcodes()
    {
        return $this->belongsToMany(Postcode::class, Location::getTableName());
    }

    public function locations()
    {
        return $this->hasMany(Location::class);
    }
}

<?php

namespace Belebo\Models\Location;

use Belebo\Traits\TableNameGetter;
use Illuminate\Database\Eloquent\Model;

class Postcode extends Model
{
    use TableNameGetter;

    protected $table = 'locations__postcodes';

    protected $fillable = ['code', 'department_id'];

    public $timestamps = false;

    public function cities()
    {
        return $this->belongsToMany(City::class, Location::getTableName());
    }

    public function locations()
    {
        return $this->hasMany(Location::class);
    }

    public function department()
    {
        return $this->belongsTo(Department::class);
    }
}

<?php

namespace Belebo\Models\Promocode;

use Illuminate\Database\Eloquent\Model;

class PromocodeType extends Model
{
    protected $table = 'promocode_types';

    protected $fillable = ['name'];

    public $timestamps  = false;
}
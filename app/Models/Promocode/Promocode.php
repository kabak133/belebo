<?php

namespace Belebo\Models\Promocode;

use Illuminate\Database\Eloquent\Model;

class Promocode extends Model
{
    protected $table = 'promocodes';

    protected $fillable = [
        'name','discount','active','from', 'to', 'type_id', 'counter'
    ];

    public $timestamps  = false;

    public function type()
    {
        return $this->belongsTo(PromocodeType::class, 'type_id');
    }
}
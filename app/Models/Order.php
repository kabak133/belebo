<?php

namespace Belebo\Models;

use Belebo\Models\User\Provider\Provider;
use Belebo\Models\User\User;
use Belebo\Models\Service\Service;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'user_id', 'provider_id',
        'categories', 'amount',
        'status', 'date',
        'gift', 'promo',
        'description', 'duration'
    ];

    public function client()
    {
        return $this->belongsTo(User::class);
    }

    public function provider()
    {
        return $this->belongsTo(Provider::class);
    }

    public function services()
    {
        return $this->belongsToMany(
            Service::class, 'orders_services',
            'order_id', 'service_id');
    }
}

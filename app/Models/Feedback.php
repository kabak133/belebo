<?php

namespace Belebo\Models;

use Belebo\Traits\TableNameGetter;
use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    use TableNameGetter;

    protected $table = 'feedbacks';

    protected $fillable = [
        'type',
        'first_name',
        'last_name',
        'email',
        'phone',
        'message'
    ];
}

<?php
namespace Belebo\Services;

use Belebo\Models\Holiday;
use Belebo\Models\Order;
use Belebo\Models\Promocode\Promocode;
use Belebo\Models\Service\Service;
use Belebo\Models\User\Provider\Provider;
use Belebo\Models\User\User;
use Belebo\Models\User\Provider\Availability;
use Carbon\Carbon;

class CheckoutService
{

    /**
     *  Get services by ids
     *
     * @param array $services_ids
     * @return array
     */
    public function getServicesByIds(array $services_ids) : array
    {
        $services = [];

        foreach ($services_ids as $id){
            $service = Service::where('id', $id)->first();
            $service['image'] = $service->getFirstMediaUrl('image');

            $services[] = $service;
        }

        return $services;
    }

    /**
     * Change date format
     *
     * @param string $format
     *
     * @return array
     */
    public function changeHolidaysDateFormat(string $format = 'd.m.Y') : array
    {
        $dates = [];

        foreach (Holiday::all() as $holiday){
            $dates[] = Carbon::parse($holiday->date)->format($format);
        }

        return $dates;
    }

    /**
     * Get the nearest providers by address id
     *
     * @param $services_ids
     * @param $fulldate
     * @param $location_id
     * @return mixed
     */
    public function getProvidersByAddressId($location_id, $fulldate, $services_ids)
    {
//        $discount = array_search(4, config('project.discount'));
        return config('project.discount');
        $date = $this->parseDate($fulldate);

        $total_minutes = $this->getTotalDurationByServicesId($services_ids);

        $available_providers = Availability::query()
            ->whereIn('provider_id', function ($query) use ($location_id){
                $query->select('id')->from(with(new Provider())->getTable())
                      ->where('location_id', '=' , $location_id);
            })
            ->get();

        foreach ($available_providers as $key => $provider){

            $check_time = $this->checkTimeByDaySchedule($provider['d'.$date['day_of_week']], $date, $total_minutes);

            if(!$check_time){
                unset($available_providers[$key]);
            };
        }
        $providers_ids = $available_providers->pluck('id');

        $providers = User::query()
                        ->whereIn('id', function ($query) use ($available_providers){
                            $query->select('user_id')->from(with(new Provider())->getTable())
                                ->whereIn('id', $available_providers);
                        })
                        ->get();

        $providers = $this->serializePhoneNumber($providers);

        if(count($providers) > 10){
            return $providers->random(10);
        }

        return $providers;
    }

    /**
     * Check if provider available at $order_time
     *
     * @param array $day_schedule_array
     * @param string $order_time
     * @param int $total_minutes
     * @return bool
     */
    public function checkTimeByDaySchedule(array $day_schedule_array, array $order_time , int $total_minutes) : bool
    {
        $day_schedule_carbon = [];

        $current_time = strtotime($order_time['hours']);

        foreach ($day_schedule_array as $key => $time){
            $day_schedule_array[$key] = strtotime($time);
            $time = explode(':', $time);
            $day_schedule_carbon[$key] = Carbon::createFromTime($time[0], $time[1]);

        }

        if(Carbon::now()->format('Y-m-d') === $order_time['day']){
            $order_time['hours'] = date('H:s', strtotime('+ 1 hour ', strtotime($order_time['hours'])));
        }

        $carbon_time = array_chunk($day_schedule_carbon, 2);
        $string_time = array_chunk($day_schedule_array, 2);

        if($current_time > $string_time[0][0] && $current_time < $string_time[0][1]){
            $diff_first = $carbon_time[0][1]->diffInMinutes($carbon_time[0][0]);

            if ($total_minutes < $diff_first){
                return true;
            };

            return false;
        }

        if($current_time > $string_time[1][0] && $current_time < $string_time[1][1]){

            $diff_second =  $carbon_time[1][1]->diffInMinutes($carbon_time[1][0]);

            if ($total_minutes < $diff_second){
                return true;
            };

            return false;
        }

        return false;
    }

    /**
     * Get day and hour
     *
     * @param boolean $current_day
     * @param $fulldate
     * @return mixed
     */
    public function parseDate($fulldate) : array
    {
        $date['full_date'] = Carbon::parse($fulldate);
        $date['day_of_week'] = Carbon::parse($fulldate)->dayOfWeek;
        $date['hours'] = Carbon::parse($fulldate)->format('H:s');
        $date['day'] = Carbon::parse($fulldate)->format('Y-m-d');

        return $date;
    }

    /**
     * Count total an amount
     *
     * @param array $services_ids
     * @param string $date
     * @param string|null $promocode
     * @return array|float|int|null
     */
    public function getServicesFinalAmount(array $services_ids , string $promocode = null, string $date = null) : int
    {
        $amount = $this->getAmountByServiceIds($services_ids);

        $final_amount = $amount;

        if($date){

            $date = Carbon::parse($date)->format('Y-m-d');
            $holiday = Holiday::where('date', $date)->first();

            if($holiday){
                //If work on holidays, + 15 per cent of total amount

                $final_amount = round(($amount + $amount / 100 * config('project.holidays_charge')), 2);
            }

        }
        if($promocode){

            $promo = Promocode::where('name', $promocode)->firstOrFail();

            $final_amount = round($amount * $promo->discount, 2);

            return $final_amount;

        } else {

            $total = count($services_ids);

            if($total > 1 && $total < 5) {
                switch ($total) {
                    case $total :
                        $discount = array_search($total, config('project.discount'));

                        $final_amount = $amount - $amount / 100 * config('project.discount.' . $total . '.percent');
                        return round($final_amount, 2);
                }
            }

            return $final_amount;
        }
    }

    /**
     * Get services' total amount
     *
     * @param array $services_ids
     * @return string
     */
    public function getAmountByServiceIds(array $services_ids) : int
    {
        $services_amount = Service::query()->whereIn('id', $services_ids)->get()->pluck('price');

        return $services_amount->sum();
    }

    /**
     * Get services' time duration
     *
     * @param boolean $in_hours
     * @param array $services_ids
     * @return string
     */
    public function getTotalDurationByServicesId(array $services_ids, $in_hours = false) : int
    {
        $services_duration = Service::query()->whereIn('id', $services_ids)->get()->pluck('duration');

        return $services_duration->sum();
    }

    /**
     * Serialize phone number
     *
     * @param $providers
     * @return mixed
     */
    public function serializePhoneNumber($providers)
    {
        foreach ($providers as $provider) {
            $provider['phone'] = '+33'. $provider['phone'];
        }

        return $providers;
    }

    /**
     * Check type of order
     *
     * @param array $infoOrder
     * @return array
     */
    public function serializeInfoOrder(array $infoOrder) : array 
    {
        $data = [];

        foreach ($infoOrder as $key => $value){
            if($key === 'date'){
                $data[$key] = $value;
            }

            if($key === 'moment'){
                $data['date'] = Carbon::now()->format('Y-m-d H:s');
            }

            if($key === 'present'){
                $data['gift'] = $value;
            }
        }

        return $data;
    }

}
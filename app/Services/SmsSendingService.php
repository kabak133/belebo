<?php
namespace Belebo\Services;

use Octopush\Api\Client;

class SmsSendingService
{
    /**
     * Send sms to providers
     *
     * @param $providers|null
     * @param $user|null
     * @return mixed
     */
    public function sendNewOrderSMS($providers, $user)
    {

        $client = new Client(env('OCTOPUSH_LOGIN'), env('OCTOPUSH_API_KEY'));
        $client->setSmsSender(env('OCTOPUSH_SENDER'));
        $client->setWithReplies();

        if($providers){
//            $client->setSmsRecipients($providers);
            $client->setSmsRecipients(['+380668685892','+380668685892']);
            return $client->send('Nouvelle demande RDV 01234 le 21/01 a 16h00
                                Coupe, Blotzheim, 68730, 28,50€ pour accepter
                                le RDV répondez OUI01234');
        }
        if($user) {
//            $client->setSmsRecipients([$user]);
            $client->setSmsRecipients(['+380668685892']);
            return $client->send('RDV01234 Pris en Compte pour le 21/01 a
                                16h00 Coupe, Blotzheim, 68730. Nous cherchons
                                un coiffeur disponible');
        }
    }
}
<?php

namespace Belebo\Http\Controllers;

use Belebo\Models\Condition;
use Belebo\Http\Resources\ConditionResource;

class ConditionController extends Controller
{
    /**
     * Display specified condition
     *
     * @param Condition $condition
     * @return ConditionResource
     */
    public function __invoke(Condition $condition)
    {
        return new ConditionResource($condition);
    }
}

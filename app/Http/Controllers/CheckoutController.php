<?php

namespace Belebo\Http\Controllers;

use Belebo\Http\Requests\ChargeRequest;
use Belebo\Http\Requests\CheckoutRequest;
use Belebo\Http\Requests\PromoRequest;
use Belebo\Http\Requests\ShowCartRequest;

use Belebo\Models\Order;
use Belebo\Models\Promocode\Promocode;
use Belebo\Services\CheckoutService;

use Belebo\Services\SmsSendingService;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;

use Stripe\Stripe;
use Stripe\Charge;
use Stripe\Account;
use Stripe\Transfer;

class CheckoutController extends Controller
{

    /**
     * Get all services by ids
     *
     * @param ShowCartRequest $request
     * @param CheckoutService $service
     * @return JsonResponse
     */
    public function showList(
        ShowCartRequest $request,
        CheckoutService $service
    )
    {
        $services = $service->getServicesByIds($request['services']);

        $holidays = $service->changeHolidaysDateFormat();

        return new JsonResponse([
            'services' => $services,
            'discount' => config('project.discount'),
            'holidays' => $holidays
        ], 200);

    }

    /**
     * Check if promo code exist in Db
     *
     * @param Promocode $promocode
     * @param PromoRequest $request
     * @return JsonResponse
     */
    public function verifyPromo(PromoRequest $request, Promocode $promocode)
    {
        $code = $promocode->where('name', $request['promo'])->firstOrFail();

        return new JsonResponse(['status' => true, 'discount' => $code->discount], 200);

    }

    /**
     * Show  charge page with data
     *
     * @param ChargeRequest $request
     * @return JsonResponse
     */
    public function showCheckout(ChargeRequest $request)
    {
        $client = Auth()->user()->load('addresses');

        $amount = $request['amount'];

        return new JsonResponse([
                'error' => false,
               'amount' => (int)$amount,
            'addresses' => (array)$client->addresses
        ],200);
    }

    /**
     * Get charge from client
     *
     * @param ChargeRequest $request
     * @param SmsSendingService $smsSendingService
     * @param CheckoutService $checkoutService
     * @return JsonResponse
     */
    public function charge(
        ChargeRequest $request,
        CheckoutService $checkoutService,
        SmsSendingService $smsSendingService
    )
    {
        Stripe::setApiKey(config('services.stripe.secret'));

        $data = $checkoutService->serializeInfoOrder($request->infoOrder);
        $data['user_id'] = $request->user()->id;
        $data['promo'] = isset($data['promo']) ? $data['promo'] : null;
        $data['description'] = isset($request->infoOrder['message']) ? $request->infoOrder['message'] : '';
        $data['duration'] = $checkoutService->getTotalDurationByServicesId($request->products);
        $data['amount'] = $checkoutService->getServicesFinalAmount(
            $request->products, $data['promo'], isset($data['date']) ? $data['date'] : null);

        if(isset($data['date'])){

            $providers = $checkoutService->getProvidersByAddressId($request->addressId, $data['date'], $request->products);

            return response()->json(['data' => $providers]);

//            if($providers) {
//                $smsSendingService->sendNewOrderSMS($providers->pluck('phone'), $request->user()->phone_number);
//            }

            $data['status'] = config('project.order_status.1');
        }

        $order = Order::create($data);
        $order->services()->sync($request->products);
//
//        return new JsonResponse(['data' => $order]);
        return new JsonResponse(['data' => $data]);


//        $charge = Charge::create([
//                    'amount' => $request['amount'] * 100,
//                  'currency' => 'eur',
//               'description' => Auth()->user()->name.', Charge',
//                    'source' => $request['stripeToken'],
//            'transfer_group' => 'my_group',
//        ]);

//        return new JsonResponse(['data' => $request->all()]);

    }

}

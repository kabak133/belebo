<?php

namespace Belebo\Http\Controllers;

use Mail;
use Belebo\Models\Workshop;
use Belebo\Mail\WorkshopMail;
use Illuminate\Http\Response;
use Belebo\Http\Requests\WorkshopRequest;

class WorkshopController extends Controller
{
    /**
     * @param WorkshopRequest $request
     * @return Response
     */
    public function __invoke(WorkshopRequest $request)
    {
        $workshop = Workshop::create($request->all());
        Mail::send(new WorkshopMail($workshop));
        return new Response(null, 204);
    }
}

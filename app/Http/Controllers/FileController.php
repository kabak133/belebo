<?php

namespace Belebo\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Belebo\Http\Requests\FileRequest;

class FileController extends Controller
{
    public function __invoke(FileRequest $request)
    {
        $id = uniqid() . '.' . $request->file->extension();
        $request->file->storeAs(null, $id, 'tmp');
        return new JsonResponse(['data' => ['id' => $id]]);
    }
}

<?php

namespace Belebo\Http\Controllers;

use Mail;
use Belebo\Models\Feedback;
use Belebo\Mail\FeedbackMail;
use Illuminate\Http\Response;
use Belebo\Http\Requests\FeedbackRequest;

class FeedbackController extends Controller
{
    /**
     * @param FeedbackRequest $request
     * @return Response
     */
    public function __invoke(FeedbackRequest $request)
    {
        $feedback = Feedback::create($request->all());
        Mail::send(new FeedbackMail($feedback));
        return new Response(null, 204);
    }
}

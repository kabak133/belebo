<?php

namespace Belebo\Http\Controllers\User\Client;

use Crypt;
use Belebo\Models\User\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Auth\Events\Registered;
use Belebo\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Belebo\Http\Requests\User\ClientRequest as RegisterRequest;

class RegisterController extends Controller
{
    use RegistersUsers;

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Handle a client registration request for the application.
     * @param RegisterRequest $request
     * @return JsonResponse
     */
    public function __invoke(RegisterRequest $request)
    {
        $user = $this->setSocial($request);

        $user['active'] = true;
        $user['role'] = 'client';
        event(new Registered($user = User::create($user)));

        return $this->registered($request, $user)
            ?: new JsonResponse(
                ['token' => $user->createToken('')->accessToken]
            );
    }

    /**
     * @param RegisterRequest $request
     * @return array
     */
    private function setSocial(RegisterRequest $request)
    {
        $user = $request->all();

        if (isset($user['token'])) {
            $token = Crypt::decrypt($user['token']);
            $user[$token['service'] . '_id'] = $token['id'];
        }

        return $user;
    }
}

<?php

namespace Belebo\Http\Controllers\User\Client;

use Auth;
use Illuminate\Http\Response;
use Belebo\Models\User\Client\Address;
use Belebo\Http\Requests\User\ClientAddressRequest;
use Belebo\Http\Resources\User\Client\AddressResource;
use Belebo\Http\Controllers\Base\User\ClientAddressController as Controller;

class AddressController extends Controller
{
    /**
     * Display a listing of the addresses.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return $this->indexAction(Auth::user());
    }

    /**
     * Store specified address
     *
     * @param ClientAddressRequest $request
     * @return AddressResource
     */
    public function store(ClientAddressRequest $request)
    {
        return $this->storeAction($request, Auth::user());
    }

    /**
     * Show specified address
     *
     * @param Address $address
     * @return AddressResource
     */
    public function show(Address $address)
    {
        return $this->showAction(Auth::id(), $address);
    }

    /**
     * Update specified address
     *
     * @param ClientAddressRequest $request
     * @param Address $address
     * @return AddressResource
     */
    public function update(ClientAddressRequest $request, Address $address)
    {
        return $this->updateAction($request, Auth::id(), $address);
    }

    /**
     * Destroy specified address
     *
     * @param Address $address
     * @return Response
     * @throws \Exception
     */
    public function destroy(Address $address)
    {
        return $this->destroyAction(Auth::id(), $address);
    }
}

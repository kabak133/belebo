<?php

namespace Belebo\Http\Controllers\User\Client;

use Auth;
use Belebo\Http\Controllers\Controller;
use Belebo\Http\Requests\User\ClientRequest;
use Belebo\Http\Resources\User\Client\ProfileResource;

class ProfileController extends Controller
{
    /**
     * Show client's profile
     *
     * @return ProfileResource
     */
    public function show()
    {
        return new ProfileResource(Auth::user());
    }

    /**
     * Update client's profile
     *
     * @param ClientRequest $request
     * @return ProfileResource
     */
    public function update(ClientRequest $request)
    {
        Auth::user()->fill($request->except('password', 'active', 'google_id', 'facebook_id', 'stripe_id'))->save();
        return new ProfileResource(Auth::user());
    }
}

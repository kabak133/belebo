<?php

namespace Belebo\Http\Controllers\User;

use App;
use Auth;
use Belebo\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;

class ProfileController extends Controller
{
    /**
     * Call role based user action
     *
     * @return mixed
     */
    public function show()
    {
        if (Auth::user()->isAdmin) {
            return new JsonResponse(['data' => ['role' => 'admin']]);
        }
        return App::call($this->getNamespace() . '\ProfileController@show');
    }

    /**
     * Call role based user action
     *
     * @return mixed
     */
    public function update()
    {
        return App::call($this->getNamespace() . '\ProfileController@update');
    }

    /**
     * Get user role based namespace
     *
     * @return string
     */
    private function getNamespace()
    {
        return __NAMESPACE__ . '\\' . ucfirst(Auth::user()->role);
    }
}

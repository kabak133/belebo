<?php

namespace Belebo\Http\Controllers\User;

use Auth;
use Hash;
use Illuminate\Http\JsonResponse;
use Belebo\Http\Controllers\Controller;
use Belebo\Http\Requests\User\UpdatePasswordRequest;

class UpdatePasswordController extends Controller
{
    /**
     * @param UpdatePasswordRequest $request
     * @return JsonResponse
     */
    public function __invoke(UpdatePasswordRequest $request)
    {
        $user = Auth::user();
        if (!$user->hasPassword || Hash::check($request->password, $user->password)) {
            $user->fill(['password' => $request->password_new])->save();
            return new JsonResponse(null, 204);
        } else {
            $data = [
                "message" => "The given data was invalid.",
                "errors" => [
                    "password" => [trans('auth.failed')]
                ]
            ];
            return new JsonResponse($data, 422);
        }
    }
}

<?php

namespace Belebo\Http\Controllers\User;

use Illuminate\Http\JsonResponse;
use Belebo\Http\Controllers\Controller;
use Belebo\Http\Requests\User\EmailExistenceRequest;

class EmailExistenceController extends Controller
{
    public function __invoke(EmailExistenceRequest $request)
    {
        return new JsonResponse(['data' => ['valid' => true]]);
    }
}

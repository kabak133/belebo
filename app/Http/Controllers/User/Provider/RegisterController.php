<?php

namespace Belebo\Http\Controllers\User\Provider;

use Auth;
use Storage;
use Request;
use Belebo\Models\User\User;
use Belebo\Models\User\Provider\{
    Availability, Provider, Company, Representative, Shareholder
};
use Illuminate\Http\JsonResponse;
use Illuminate\Auth\Events\Registered;
use Belebo\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Belebo\Http\Requests\User\ProviderRegisterRequest as RegisterRequest;

class RegisterController extends Controller
{
    use RegistersUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        if (Auth::check() && Auth::user()->is_admin) {
            $this->middleware('admin');
        } else {
            $this->middleware('guest');
        }
    }

    /**
     * Handle a provider registration request for the application.
     *
     * @param RegisterRequest $request
     * @return JsonResponse
     */
    public function __invoke(RegisterRequest $request)
    {
        $user = $this->setUser($request->provider);
        $provider = $this->setProvider($user, $request->provider);
        $this->setCompany($provider, $request->company);
        $this->setRepresentative($provider, $request->representative);
        $this->setShareholders($provider, $request->shareholders);
        $provider->availability()->save(new Availability());

        if (Auth::check() && Auth::user()->is_admin) {
            $response = new JsonResponse(null, 204);
        } else {
            $response = new JsonResponse(['data' => ['id' => $user->id]], 201);
        }


        $response = new JsonResponse(['data' => ['id' => $user->id]], 201);


        return $this->registered($request, $user)
            ?: $response;
    }

    private function setUser($user)
    {
        $user['active'] = false;
        $user['role'] = 'provider';
        event(new Registered($user = User::create($user)));

        return $user;
    }

    private function setProvider(User $user, array $data)
    {
        $provider = $user->provider()->save(
            new Provider(array_merge($data, ['ip' => Request::ip()]))
        );
        $provider->addMedia(Storage::disk('tmp')->path($data['passport']))
            ->toMediaCollection('passport');
        return $provider;
    }

    private function setCompany(Provider $provider, array $data)
    {
        $company = $provider->company()->save(new Company($data));
        $company->addMedia(Storage::disk('tmp')->path($data['certificate']))
            ->toMediaCollection('certificate');
    }

    private function setRepresentative(Provider $provider, array $data)
    {
        $representative = $provider->representative()->save(new Representative($data));
        $representative->addMedia(Storage::disk('tmp')->path($data['passport']))
            ->toMediaCollection('passport');
    }

    private function setShareholders(Provider $provider, $shareholders)
    {
        if ($shareholders) {
            foreach ($shareholders as $data) {
                $shareholder = $provider->shareholders()->save(new Shareholder($data));
                $shareholder->addMedia(Storage::disk('tmp')->path($data['passport']))
                    ->toMediaCollection('passport');
            }
        }
    }
}

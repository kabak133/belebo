<?php

namespace Belebo\Http\Controllers\User\Provider;

use Auth;
use Belebo\Http\Controllers\Controller;
use Belebo\Http\Requests\User\ProviderRequest;
use Belebo\Http\Resources\User\Provider\ProfileResource;

class ProfileController extends Controller
{
    /**
     * Show provider's profile
     *
     * @return ProfileResource
     */
    public function show()
    {
        return new ProfileResource(Auth::user());
    }

    /**
     * Update provider's profile
     *
     * @param ProviderRequest $request
     * @return ProfileResource
     */
    public function update(ProviderRequest $request)
    {
        //TODO: Remove next line after testing
        \Belebo\Models\User\Provider\Company::flushEventListeners();

        $user = Auth::user();
        if ($request->profile) {
            $user->update($request->profile);
        }
        if ($request->password) {
            $user->fill($request->password);
        }
        if ($request->photo) {
            $user->provider->addMediaFromRequest('photo')->toMediaCollection('photo');
        }
        if ($request->provider) {
            $user->provider->update($request->provider);
        }
        if ($request->services) {
            $user->provider->services()->sync($request->services);
        }
        if ($request->availability) {
            $user->provider->availability->update($request->availability);
        }
        if ($request->company) {
            $user->provider->company->update($request->company);
        }
        $user->save();
        return new ProfileResource($user);
    }
}

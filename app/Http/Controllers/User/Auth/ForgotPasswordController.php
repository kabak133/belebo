<?php

namespace Belebo\Http\Controllers\User\Auth;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Belebo\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class ForgotPasswordController extends Controller
{
    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Return response for a successful password reset link.
     *
     * @param  string $response
     * @return JsonResponse
     */
    protected function sendResetLinkResponse($response)
    {
        return new JsonResponse(['status' => trans($response)]);
    }

    /**
     * Return response for a failed password reset link.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  string $response
     * @return JsonResponse
     */
    protected function sendResetLinkFailedResponse(Request $request, $response)
    {
        $data = [
            "message" => "The given data was invalid.",
            "errors" => [
                "email" => [trans($response)]
            ]
        ];

        return new JsonResponse($data, 422);
    }
}

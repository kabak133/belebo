<?php

namespace Belebo\Http\Controllers\User\Auth;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Belebo\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;

class ResetPasswordController extends Controller
{
    use ResetsPasswords;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get the response for a successful password reset.
     *
     * @param  string $response
     * @return JsonResponse
     */
    protected function sendResetResponse($response)
    {
        return new JsonResponse(['status' => trans($response)]);
    }

    /**
     * Get the response for a failed password reset.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  string $response
     * @return JsonResponse
     */
    protected function sendResetFailedResponse(Request $request, $response)
    {
        $data = [
            "message" => "The given data was invalid.",
            "errors" => [
                "email" => [trans($response)]
            ]
        ];

        return new JsonResponse($data, 422);
    }
}

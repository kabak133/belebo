<?php

namespace Belebo\Http\Controllers\User\Auth;

use Auth;
use Crypt;
use Socialite;
use Belebo\Models\User\User;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Belebo\Http\Controllers\Controller;
use Belebo\Http\Requests\User\LoginRequest;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout', 'checkAuth');
    }

    public function getProviderUrl($service)
    {
        $url = Socialite::driver($service)->stateless()->redirect()->getTargetUrl();
        return new JsonResponse(['url' => $url]);
    }

    public function getProvidersUrls()
    {
        $facebook = Socialite::driver('facebook')->stateless()->redirect()->getTargetUrl();
        $google = Socialite::driver('google')->stateless()->redirect()->getTargetUrl();
        return new JsonResponse([
            'facebook' => $facebook,
            'google' => $google
        ]);
    }

    /**
     * Authorize or return registration data on social login
     *
     * @param $service
     * @return JsonResponse
     */
    public function handleProviderCallback($service)
    {
        $data = \Socialite::driver($service)->stateless()->user();
        $user = User::where($service . '_id', $data->id)->first();
        if ($user) {
            return $this->activeUserToken($user);
        } else {
            $result['token'] = Crypt::encrypt(['service' => $service, 'id' => $data->id]);
            if ($service == 'google') {
                $result['first_name'] = $data->user['name']['givenName'];
                $result['last_name'] = $data->user['name']['familyName'];
            } elseif ($service == 'facebook') {
                $result['first_name'] = str_before($data->user['name'], ' ');
                $result['last_name'] = str_after($data->user['name'], ' ');
            }
            $result['email'] = $data->email;
            return new JsonResponse($result);
        }
    }

    /**
     * Handle a login request to the application.
     *
     * @param LoginRequest $request
     * @return JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(LoginRequest $request)
    {
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            return $this->authenticated($request, $this->guard()->user())
                ?: $this->activeUserToken(Auth::user());
//            $this->activeUserToken(\Auth::user());
        }
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    /**
     * Handle a logout request to the application.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        $request->user()->token()->delete();
        return new JsonResponse(null, 204);
    }

    /**
     * Check user auth
     *
     * @return JsonResponse
     */
    public function checkAuth()
    {
        return new JsonResponse(null, 204);
    }

    /**
     * @param User $user
     * @return JsonResponse
     */
    private function activeUserToken(User $user)
    {
        if ($user->active) {
            return new JsonResponse(
                ['token' => $user->createToken('')->accessToken]
            );
        } else {
            return abort('403');
        }
    }
}

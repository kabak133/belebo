<?php

namespace Belebo\Http\Controllers;

use Belebo\Models\Question\Category;
use Belebo\Http\Resources\Question\CategoryResource;

class QuestionController extends Controller
{
    /**
     * Show all questions with categories
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function __invoke()
    {
        return CategoryResource::collection(Category::with('questions')->get());
    }
}

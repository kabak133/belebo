<?php

namespace Belebo\Http\Controllers;

use Belebo\Models\Service\Service;
use Belebo\Models\Service\Category;
use Belebo\Http\Resources\Service\ServiceResource;
use Belebo\Http\Resources\Service\CategoryResource;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return CategoryResource::collection(Category::with('services')->orderBy('id')->get());
    }

    /**
     * Display the specified resource.
     *
     * @param Service $service
     * @return ServiceResource
     */
    public function show($service)
    {
        return new ServiceResource(Service::whereName($service)->first());
    }
}

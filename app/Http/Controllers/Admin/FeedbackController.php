<?php

namespace Belebo\Http\Controllers\Admin;

use Belebo\Models\Feedback;
use Illuminate\Http\Response;
use Belebo\Http\Controllers\Controller;
use Belebo\Http\Resources\Admin\FeedbackResource;

class FeedbackController extends Controller
{
    /**
     * Get feedback listing
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return FeedbackResource::collection(Feedback::latest()->paginate());
    }

    /**
     * Delete specified feedback from storage
     *
     * @param Feedback $feedback
     * @return Response
     * @throws \Exception
     */
    public function destroy(Feedback $feedback)
    {
        $feedback->delete();
        return new Response(null, 204);
    }
}

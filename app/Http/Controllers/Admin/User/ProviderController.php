<?php

namespace Belebo\Http\Controllers\Admin\User;

use Belebo\Models\User\User;
use Illuminate\Http\Response;
use Belebo\Http\Controllers\Controller;
use Belebo\Http\Requests\User\ProviderRequest;
use Belebo\Http\Resources\Admin\User\ProviderResource;
use Belebo\Http\Resources\Admin\User\UserListResource;

class ProviderController extends Controller
{
    /**
     * Display a listing of the providers.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return UserListResource::collection(User::providers()->orderBy('updated_at', 'desc')->paginate());
    }

    /**
     * Display the specified provider.
     *
     * @param User $user
     * @return ProviderResource
     */
    public function show(User $user)
    {
        $this->checkRole($user);
        return new ProviderResource($user);
    }

    /**
     * Update the specified provider in storage.
     *
     * @param ProviderRequest $request
     * @param User $user
     * @return ProviderResource
     */
    public function update(ProviderRequest $request, User $user)
    {
        $this->checkRole($user);
        if ($request->profile) {
            $user->update($request->profile);
        }
        if ($request->password) {
            $user->fill($request->password);
        }
        if (!is_null($request->active)) {
            $user->active = $request->active;
        }
        if ($request->photo) {
            $user->provider->addMediaFromRequest('photo')->toMediaCollection('photo');
        }
        if ($request->provider) {
            $user->provider->update($request->provider);
        }
        if ($request->services) {
            $user->provider->services()->sync($request->services);
        }
        if ($request->availability) {
            $user->provider->availability->update($request->availability);
        }
        if ($request->company) {
            $user->provider->company->update($request->company);
        }
        $user->save();
        return new ProviderResource($user);
    }

    /**
     * Remove the specified provider from storage.
     * Deleting all parts separately for media library cascade action
     *
     * @param User $user
     * @return Response
     * @throws \Exception
     */
    public function destroy(User $user)
    {
        $this->checkRole($user);
        foreach ($user->provider->shareholders as $shareholder) {
            $shareholder->delete();
        }
        $user->provider->representative->delete();
        $user->provider->company->delete();
        $user->provider->delete();
        $user->delete();
        return new Response(null, 204);
    }

    /**
     * Throw error if user is not a provider
     *
     * @param User $user
     */
    private function checkRole(User $user)
    {
        if (!$user->isProvider) {
            abort(404);
        }
    }
}

<?php

namespace Belebo\Http\Controllers\Admin\User;

use Belebo\Models\User\User;
use Illuminate\Http\Response;
use Belebo\Models\User\Client\Address;
use Belebo\Http\Requests\User\ClientAddressRequest;
use Belebo\Http\Resources\User\Client\AddressResource;
use Belebo\Http\Controllers\Base\User\ClientAddressController as Controller;

class ClientAddressController extends Controller
{

    /**
     * Display a listing of the addresses.
     *
     * @param User $user
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(User $user)
    {
        return $this->indexAction($user);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ClientAddressRequest $request
     * @param User $user
     * @return AddressResource
     */
    public function store(ClientAddressRequest $request, User $user)
    {
        return $this->storeAction($request, $user);
    }

    /**
     * Update the specified address
     *
     * @param ClientAddressRequest $request
     * @param int $user
     * @param Address $address
     * @return AddressResource
     */
    public function update(ClientAddressRequest $request, int $user, Address $address)
    {
        return $this->updateAction($request, $user, $address);
    }

    /**
     * Destroy specified address
     *
     * @param int $user
     * @param Address $address
     * @return Response
     * @throws \Exception
     */
    public function destroy(int $user, Address $address)
    {
        return $this->destroyAction($user, $address);
    }

}

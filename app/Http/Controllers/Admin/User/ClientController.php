<?php

namespace Belebo\Http\Controllers\Admin\User;

use Belebo\Models\User\User;
use Illuminate\Http\Response;
use Belebo\Http\Controllers\Controller;
use Belebo\Http\Requests\User\ClientRequest;
use Belebo\Http\Resources\Admin\User\ClientResource;
use Belebo\Http\Resources\Admin\User\UserListResource;

class ClientController extends Controller
{
    /**
     * Display a listing of the clients.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return UserListResource::collection(User::customers()->orderBy('updated_at', 'desc')->paginate());
    }

    /**
     * Store a client in storage.
     *
     * @param ClientRequest $request
     * @return ClientResource
     */
    public function store(ClientRequest $request)
    {
        return new ClientResource(User::create($request->all() + ['role' => 'client']));
    }

    /**
     * Display the specified client.
     *
     * @param User $user
     * @return ClientResource
     */
    public function show(User $user)
    {
        $this->checkRole($user);
        return new ClientResource($user);
    }

    /**
     * Update specified client
     *
     * @param ClientRequest $request
     * @param User $user
     * @return ClientResource|Response
     */
    public function update(ClientRequest $request, User $user)
    {
        $this->checkRole($user);
        $user->fill($request->all())->save();
        return new ClientResource($user);
    }

    /**
     * Remove specified client
     *
     * @param User $user
     * @return Response
     * @throws \Exception
     */
    public function destroy(User $user)
    {
        $this->checkRole($user);
        $user->delete();
        return new Response(null, 204);
    }

    /**
     * Throw error if user is not a client
     *
     * @param User $user
     */
    private function checkRole(User $user)
    {
        if (!$user->isClient) {
            abort(404);
        }
    }
}

<?php

namespace Belebo\Http\Controllers\Admin\News;

use Storage;
use Illuminate\Http\Response;
use Belebo\Http\Controllers\Controller;
use Belebo\Models\News\Category,
    Belebo\Http\Requests\Admin\News\CategoryRequest,
    Belebo\Http\Resources\News\BaseCategoryResource as CategoryResource;

class CategoryController extends Controller
{
    /**
     * Get all news categories
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return CategoryResource::collection(Category::all());
    }

    /**
     * Store category
     *
     * @param CategoryRequest $request
     * @return CategoryResource
     */
    public function store(CategoryRequest $request)
    {
        $request = $request->only('name', 'slug');
        $request['slug'] = str_slug($request['slug'] ?? null);

        return new CategoryResource(Category::create($request));
    }

    /**
     * Update category
     *
     * @param CategoryRequest $request
     * @param Category $category
     * @return CategoryResource
     */
    public function update(CategoryRequest $request, Category $category)
    {
        $request = $request->only('name', 'slug');
        if (empty($request['slug']))
            $request['slug'] = str_slug($request['name']);
        else
            $request['slug'] = str_slug($request['slug']);
        $category->update($request);
        $category->save();

        return new CategoryResource($category);
    }

    /**
     * Remove category with all related news
     *
     * @param Category $category
     * @return Response
     * @throws \Exception
     */
    public function destroy(Category $category)
    {
        $images = [];
        $news = $category->news;
        foreach ($news as $article) {
            $images[] = $article->image;
            if ($article->image_v) {
                $images[] = $article->image_v;
            }
        }
        Storage::delete($images);
        $category->delete();

        return new Response(null, 204);
    }
}

<?php

namespace Belebo\Http\Controllers\Admin\News;

use Illuminate\Http\Response;
use Belebo\Http\Controllers\Controller;
use Belebo\Models\News\Tag,
    Belebo\Http\Requests\Admin\News\TagRequest,
    Belebo\Http\Resources\News\BaseCategoryResource as TagResource;

class TagController extends Controller
{
    /**
     * Get all news tags
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return TagResource::collection(Tag::all());
    }

    /**
     * Store tag
     *
     * @param TagRequest $request
     * @return TagResource
     */
    public function store(TagRequest $request)
    {
        $request = $request->only('name', 'slug');
        $request['slug'] = str_slug($request['slug'] ?? null);

        return new TagResource(Tag::create($request));
    }

    /**
     * Update tag
     *
     * @param TagRequest $request
     * @param Tag $tag
     * @return TagResource
     */
    public function update(TagRequest $request, Tag $tag)
    {
        $request = $request->only('name', 'slug');
        if (empty($request['slug']))
            $request['slug'] = str_slug($request['name']);
        else
            $request['slug'] = str_slug($request['slug']);
        $tag->update($request);
        $tag->save();

        return new TagResource($tag);
    }

    /**
     * Remove tag
     *
     * @param Tag $tag
     * @return Response
     * @throws \Exception
     */
    public function destroy(Tag $tag)
    {
        $tag->delete();

        return new Response(null, 204);
    }
}

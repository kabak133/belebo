<?php

namespace Belebo\Http\Controllers\Admin\News;

use Storage;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Belebo\Http\Controllers\Controller;
use Belebo\Models\News\News,
    Belebo\Http\Requests\Admin\News\NewsRequest,
    Belebo\Http\Resources\News\NewsResource;

class NewsController extends Controller
{
    /**
     * Get all news
     *
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        return NewsResource::collection(
            News::with('category')->latest()
                ->paginate($request->query('limit') ?? 10)
        );
    }

    /**
     * Get news item by id
     *
     * @param News $news
     * @return NewsResource
     */
    public function show(News $news)
    {
        return new NewsResource($news->load('category'));
    }

    /**
     * Store news item
     *
     * @param NewsRequest $request
     * @return NewsResource
     */
    public function store(NewsRequest $request)
    {
        $news = News::create($request->all());
        $news->addMedia(Storage::disk('tmp')->path($request->image))
            ->toMediaCollection('horizontal');
        if ($request->image_v) {
            $news->addMedia(Storage::disk('tmp')->path($request->image_v))
                ->toMediaCollection('vertical');
        }
        $news->tags()->sync($request->tags ?? []);

        return new NewsResource($news);
    }

    /**
     * Update specified news item
     *
     * @param NewsRequest $request
     * @param News $news
     * @return NewsResource
     */
    public function update(NewsRequest $request, News $news)
    {
        $news->update($request->all());
        if ($request->image) {
            $news->addMediaFromRequest('image')->toMediaCollection('horizontal');
        }
        if ($request->image_v) {
            $news->addMediaFromRequest('image_v')->toMediaCollection('vertical');
        }
        $news->tags()->sync($request->tags ?? []);

        return new NewsResource($news->load('tags'));
    }

    /**
     * Remove news item
     *
     * @param News $news
     * @return Response
     * @throws \Exception
     */
    public function destroy(News $news)
    {
        $news->delete();

        return new Response(null, 204);
    }
}

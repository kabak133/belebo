<?php

namespace Belebo\Http\Controllers\Admin;

use Belebo\Models\Workshop;
use Illuminate\Http\Response;
use Belebo\Http\Controllers\Controller;
use Belebo\Http\Resources\Admin\WorkshopResource;

class WorkshopController extends Controller
{
    /**
     * Get workshop reqiests
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return WorkshopResource::collection(Workshop::latest()->paginate());
    }

    /**
     * Remove the specified workshop request from storage.
     *
     * @param Workshop $workshop
     * @return Response
     * @throws \Exception
     */
    public function destroy(Workshop $workshop)
    {
        $workshop->delete();
        return new Response(null, 204);
    }
}

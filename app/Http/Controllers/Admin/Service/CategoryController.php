<?php

namespace Belebo\Http\Controllers\Admin\Service;

use Belebo\Models\Service\{
    Category, Service
};
use Illuminate\Http\Response;
use Belebo\Http\Controllers\Controller;
use Belebo\Http\Requests\Admin\Service\CategoryRequest;
use Belebo\Http\Resources\BaseCategoryResource as CategoryResource;

class CategoryController extends Controller
{
    /**
     *  Display a listing of the categories.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return CategoryResource::collection(Category::all());
    }

    /**
     * Store a newly created category in storage.
     *
     * @param CategoryRequest $request
     * @return CategoryResource
     */
    public function store(CategoryRequest $request)
    {
        return new CategoryResource(Category::create($request->all()));
    }

    /**
     * Update the specified category in storage.
     *
     * @param CategoryRequest $request
     * @param Category $category
     * @return CategoryResource
     */
    public function update(CategoryRequest $request, Category $category)
    {
        $category->fill($request->all())->save();
        return new CategoryResource($category);
    }

    /**
     * Remove the specified category from storage.
     *
     * @param Category $category
     * @return Response
     * @throws \Exception
     */
    public function destroy(Category $category)
    {
        $services = $category->services()->pluck('id');
        Service::destroy($services->all());
        $category->delete();
        return new Response(null, 204);
    }
}

<?php

namespace Belebo\Http\Controllers\Admin\Service;

use Storage;
use Illuminate\Http\Response;
use Belebo\Models\Service\Service;
use Belebo\Http\Controllers\Controller;
use Belebo\Http\Resources\Service\ServiceResource;
use Belebo\Http\Requests\Admin\Service\ServiceRequest;

class ServiceController extends Controller
{
    /**
     * Display a listing of the services.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return ServiceResource::collection(Service::with('category')->paginate());
    }

    /**
     * Get news item by id
     *
     * @param Service $service
     * @return ServiceResource
     */
    public function show(Service $service)
    {
        return new ServiceResource($service->load('category'));
    }

    /**
     * Store a newly created service in storage.
     *
     * @param ServiceRequest $request
     * @return ServiceResource
     */
    public function store(ServiceRequest $request)
    {
        $service = Service::create($request->all());
        $service->addMedia(Storage::disk('tmp')->path($request->image))
            ->toMediaCollection('image');

        return new ServiceResource($service);
    }

    /**
     * Update the specified service in storage.
     *
     * @param ServiceRequest $request
     * @param Service $service
     * @return ServiceResource
     */
    public function update(ServiceRequest $request, Service $service)
    {
        if ($request->image) {
            $service->addMediaFromRequest('image')->toMediaCollection('image');
        }
        $service->fill($request->all())->save();
        return new ServiceResource($service);
    }

    /**
     * Remove the specified service from storage.
     *
     * @param Service $service
     * @return Response
     * @throws \Exception
     */
    public function destroy(Service $service)
    {
        $service->delete();
        return new Response(null, 204);
    }
}

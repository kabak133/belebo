<?php

namespace Belebo\Http\Controllers\Admin;

use Belebo\Http\Requests\Admin\HolidayRequest;
use Belebo\Http\Resources\Admin\HolidayResource;
use Belebo\Models\Holiday;

class HolidayController
{
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return HolidayResource::collection(Holiday::all());
    }


    /**
     * Create a new holiday day
     *
     * @param HolidayRequest $request
     * @return HolidayResource
     */
    public function store(HolidayRequest $request)
    {
        $holiday = Holiday::create($request->all());

        return new HolidayResource($holiday);
    }

    /**
     * Update holiday data by id
     *
     * @param HolidayRequest $request
     * @param Holiday $holiday
     * @return HolidayResource
     */
    public function update(HolidayRequest $request, Holiday $holiday)
    {
        $holiday->update($request->all());

        return new HolidayResource($holiday);
    }

    /**
     * Delete holiday
     *
     * @param Holiday $holiday
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function destroy(Holiday $holiday)
    {
        $holiday->delete();

        return response(null, 204);
    }
}
<?php

namespace Belebo\Http\Controllers\Admin\Question;

use Illuminate\Http\Response;
use Belebo\Models\Question\Question;
use Belebo\Http\Controllers\Controller;
use Belebo\Http\Resources\Question\QuestionResource;
use Belebo\Http\Requests\Admin\Question\QuestionRequest;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return QuestionResource::collection(Question::with('category')->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param QuestionRequest $request
     * @return QuestionResource
     */
    public function store(QuestionRequest $request)
    {
        return new QuestionResource(Question::create($request->all()));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param QuestionRequest $request
     * @param Question $question
     * @return QuestionResource
     */
    public function update(QuestionRequest $request, Question $question)
    {
        $question->fill($request->all())->save();
        return new QuestionResource($question);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Question $question
     * @return Response
     * @throws \Exception
     */
    public function destroy(Question $question)
    {
        $question->delete();
        return new Response(null, 204);
    }
}

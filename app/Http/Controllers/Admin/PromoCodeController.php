<?php

namespace Belebo\Http\Controllers\Admin;

use Belebo\Http\Controllers\Controller;
use Belebo\Http\Requests\Admin\PromoCodeRequest;
use Belebo\Http\Resources\PromoCodeResource;
use Illuminate\Http\Response;
use Belebo\Models\Promocode\Promocode;

class PromoCodeController extends Controller
{

    /**
     * Show all codes
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return PromoCodeResource::collection(
            Promocode::with('type')->get()
        );
    }

    /**
     * @param PromoCode $promocode
     * @return PromoCodeResource
     */
    public function show(PromoCode $promocode)
    {
        return new PromoCodeResource($promocode);
    }

    /**
     * Create new code
     *
     * @param PromoCodeRequest $request
     * @return PromoCodeResource
     */
    public function store(PromoCodeRequest $request)
    {
        $code = Promocode::create($request->all());

        return new PromoCodeResource($code);
    }

    /**
     * Update code
     *
     * @param PromoCodeRequest $request
     * @param PromoCode $promocode
     * @return PromoCodeResource
     */
    public function update(PromoCodeRequest $request, Promocode $promocode)
    {
        $promocode->update($request->all());

        return new PromoCodeResource($promocode);
    }

    /**
     * @param PromoCode $promocode
     * @return Response
     * @throws \Exception
     */
    public function destroy(Promocode $promocode)
    {
        $promocode->delete();

        return new Response(null, 204);
    }
}

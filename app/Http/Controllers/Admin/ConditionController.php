<?php

namespace Belebo\Http\Controllers\Admin;

use Belebo\Models\Condition;
use Belebo\Http\Controllers\Controller;
use Belebo\Http\Resources\ConditionResource;
use Belebo\Http\Requests\Admin\ConditionRequest;

class ConditionController extends Controller
{
    /**
     * Display specified condition
     *
     * @param Condition $condition
     * @return ConditionResource
     */
    public function show(Condition $condition)
    {
        return new ConditionResource($condition);
    }

    /**
     * Update the specified condition in storage.
     *
     * @param ConditionRequest $request
     * @param Condition $condition
     * @return ConditionResource
     */
    public function update(ConditionRequest $request, Condition $condition)
    {
        $condition->fill($request->all())->save();
        return new ConditionResource($condition);
    }
}

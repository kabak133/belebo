<?php

namespace Belebo\Http\Controllers\Admin\Location;

use Belebo\Models\Location\Postcode;
use Belebo\Http\Controllers\Controller;
use Belebo\Http\Resources\Admin\Location\PostcodeResource;

class PostcodeController extends Controller
{
    public function __invoke()
    {
        return PostcodeResource::collection(Postcode::with('department')->get());
    }
}

<?php

namespace Belebo\Http\Controllers\Admin\Location;

use Belebo\Models\Location\{
    City, Postcode, Location
};
use Illuminate\Http\Response;
use Belebo\Http\Controllers\Controller;
use Belebo\Http\Requests\Admin\Location\LocationRequest;
use Belebo\Http\Resources\Admin\Location\LocationResource;

class LocationController extends Controller
{
    /**
     * Display a listing of the locations.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return LocationResource::collection(Location::paginate());
    }

    /**
     * Store a newly created location in storage.
     *
     * @param LocationRequest $request
     * @return LocationResource
     */
    public function store(LocationRequest $request)
    {
        $location = $this->updateLocation(new Location(), $request);
        return new LocationResource($location);
    }

    /**
     * Update the specified location in storage.
     *
     * @param LocationRequest $request
     * @param Location $location
     * @return LocationResource
     */
    public function update(LocationRequest $request, Location $location)
    {
        $location = $this->updateLocation($location, $request);
        $this->clearCitiesAndPostcodes();
        return new LocationResource($location);
    }

    /**
     * Remove the specified location from output.
     *
     * @param Location $location
     * @return Response
     * @throws \Exception
     */
    public function destroy(Location $location)
    {
        //TODO: check location usage before delete
        $location->delete();
        $this->clearCitiesAndPostcodes();

        return new Response(null, 204);
    }

    private function updateLocation(Location $location, LocationRequest $request): Location
    {
        $location->city_id = $request->city_id ?? City::firstOrCreate(['name' => $request->city])->id;
        if ($request->postcode_id) {
            $postcode = Postcode::find($request->postcode_id);
            if ($request->department_id && $postcode->department_id != $request->department_id) {
                $postcode->department_id = $request->department_id;
                $postcode->save();
            }
        } else {
            $postcode = Postcode::create(['code' => $request->postcode, 'department_id' => $request->department_id]);
        }
        $location->postcode_id = $postcode->id;
        $location->save();
        $this->clearCitiesAndPostcodes();

        return $location;
    }

    private function clearCitiesAndPostcodes()
    {
        $postodes = Postcode::doesntHave('cities')->pluck('id');
        Postcode::destroy($postodes->all());
        $cities = City::doesntHave('postcodes')->pluck('id');
        City::destroy($cities->all());
    }
}

<?php

namespace Belebo\Http\Controllers\Admin\Location;

use Belebo\Models\Location\City;
use Belebo\Http\Controllers\Controller;
use Belebo\Http\Resources\Admin\Location\CityResource;

class CityController extends Controller
{
    public function __invoke()
    {
        return CityResource::collection(City::all());
    }
}

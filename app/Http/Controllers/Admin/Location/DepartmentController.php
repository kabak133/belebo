<?php

namespace Belebo\Http\Controllers\Admin\Location;

use Illuminate\Http\Response;
use Belebo\Models\Location\Department;
use Belebo\Http\Controllers\Controller;
use Belebo\Http\Requests\Admin\Location\DepartmentRequest;
use Belebo\Http\Resources\Admin\Location\DepartmentResource;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the departments.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return DepartmentResource::collection(Department::all());
    }

    /**
     * Store a newly created department in storage.
     *
     * @param DepartmentRequest $request
     * @return DepartmentResource
     */
    public function store(DepartmentRequest $request)
    {
        return new DepartmentResource(Department::create($request->all()));
    }

    /**
     * Update the specified department in storage.
     *
     * @param DepartmentRequest $request
     * @param Department $department
     * @return DepartmentResource
     */
    public function update(DepartmentRequest $request, Department $department)
    {
        $department->fill($request->all())->save();
        return new DepartmentResource($department);
    }

    /**
     * Remove the specified department from output.
     *
     * @param Department $department
     * @return Response
     * @throws \Exception
     */
    public function destroy(Department $department)
    {
        if ($department->postcodes->isEmpty()) {
            $department->delete();
            return new Response(null, 204);
        } else {
            abort(409,'Department has connected locations. Please, remove connected locations first.');
        }
    }
}

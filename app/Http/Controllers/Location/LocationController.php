<?php

namespace Belebo\Http\Controllers\Location;

use Belebo\Models\Location\Location;
use Belebo\Http\Controllers\Controller;
use Belebo\Http\Resources\Location\LocationResource;

class LocationController extends Controller
{
    /**
     * Find location by postcode or city name.
     *
     * @param  string $location
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function __invoke($location)
    {
        if (is_numeric($location)) {
            return LocationResource::collection(Location::findByCode($location)->get());
        } else {
            return LocationResource::collection(Location::findByCity($location)->get());
        }
    }
}

<?php

namespace Belebo\Http\Controllers\Location;

use Belebo\Models\Location\Department;
use Belebo\Http\Controllers\Controller;
use Belebo\Http\Resources\Location\DepartmentResourceCollection;

class DepartmentController extends Controller
{
    /**
     * List of available and unavailable departments
     *
     * @return DepartmentResourceCollection
     */
    public function __invoke()
    {
        return new DepartmentResourceCollection(Department::all());
    }
}

<?php

namespace Belebo\Http\Controllers\News;

use Illuminate\Http\Request;
use Belebo\Models\News\News;
use Belebo\Http\Controllers\Controller;
use Belebo\Http\Resources\News\NewsResource;

class NewsController extends Controller
{
    /**
     * Get all published news
     *
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        return NewsResource::collection(
            News::filteredByDate($request->query('date'))
                ->published()
                ->paginate($request->query('limit') ?? 8)
        );
    }

    /**
     * Get published news item by slug
     *
     * @param string $news
     * @return mixed
     */
    public function show(string $news)
    {
        return new NewsResource(News::findPublishedBySlug($news));
    }
}

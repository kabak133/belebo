<?php

namespace Belebo\Http\Controllers\News;

use Illuminate\Http\Request;
use Belebo\Models\News\News;
use Belebo\Http\Controllers\Controller;
use Belebo\Http\Resources\News\NewsResource;

class TagController extends Controller
{
    /**
     * Get news having specified tag
     *
     * @param Request $request
     * @param $tag
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function __invoke(Request $request, $tag)
    {
        return NewsResource::collection(
            News::filteredByDate($request->query('date'))
                ->findPublishedByTag($tag)
                ->paginate($request->query('limit') ?? 8)
        );
    }
}

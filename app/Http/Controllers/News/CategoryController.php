<?php

namespace Belebo\Http\Controllers\News;

use Illuminate\Http\Request;
use Belebo\Models\News\News;
use Belebo\Models\News\Category;
use Belebo\Http\Controllers\Controller;
use Belebo\Http\Resources\News\NewsResource;
use Belebo\Http\Resources\News\BaseCategoryResource as CategoryResource;

class CategoryController extends Controller
{
    /**
     * Get all categories
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return CategoryResource::collection(Category::all());
    }

    /**
     * Get news from specified category
     *
     * @param Request $request
     * @param string $category
     * @return NewsResource|\Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function show(Request $request, $category)
    {
        if ($request->query('first')) {
            return new NewsResource(
                News::findPublishedByCategory($category)->first()
            );
        } else {
            return NewsResource::collection(
                News::filteredByDate($request->query('date'))
                    ->findPublishedByCategory($category)
                    ->paginate($request->query('limit') ?? 8)
            );
        }
    }
}

<?php

namespace Belebo\Http\Controllers\Base\User;

use Belebo\Models\User\User;
use Illuminate\Http\Response;
use Belebo\Models\User\Client\Address;
use Belebo\Http\Controllers\Controller;
use Belebo\Http\Requests\User\ClientAddressRequest;
use Belebo\Http\Resources\User\Client\AddressResource;

class ClientAddressController extends Controller
{
    /**
     * Display a listing of the addresses.
     *
     * @param User $user
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    protected function indexAction(User $user)
    {
        return AddressResource::collection($user->addresses);
    }

    /**
     * Store specified address
     *
     * @param ClientAddressRequest $request
     * @param User $user
     * @return AddressResource
     */
    protected function storeAction(ClientAddressRequest $request, User $user)
    {
        return new AddressResource(
            $user->addresses()->save(new Address($request->all()))
        );
    }

    /**
     * Update specified address
     *
     * @param ClientAddressRequest $request
     * @param int $user
     * @param Address $address
     * @return AddressResource
     */
    protected function updateAction(ClientAddressRequest $request, int $user, Address $address)
    {
        $this->checkAddress($user, $address);
        $address->fill($request->all())->save();
        return new AddressResource($address);
    }

    /**
     * Destroy address address
     *
     * @param int $user
     * @param Address $address
     * @return Response
     * @throws \Exception
     */
    protected function destroyAction(int $user, Address $address)
    {
        $this->checkAddress($user, $address);
        $address->delete();
        return new Response(null, 204);
    }

    /**
     * Abort when address doesn't belong to client
     *
     * @param int $user
     * @param Address $address
     */
    private function checkAddress(int $user, Address $address)
    {
        if ($address->user_id != $user) {
            abort(404);
        }
    }
}

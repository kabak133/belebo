<?php

namespace Belebo\Http\Resources\Question;

use Illuminate\Http\Resources\Json\Resource;

class QuestionResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            $this->mergeWhen($request->is('*/admin/*'), [
                'id' => $this->id,
                'category' => $this->category
            ]),
            'question' => $this->question,
            'answer' => $this->answer,
        ];
    }
}

<?php

namespace Belebo\Http\Resources\Question;

use Illuminate\Http\Resources\Json\Resource;

class CategoryResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            $this->mergeWhen($request->is('*/admin/*'), [
                'id' => $this->id
            ]),
            'name' => $this->name,
            'questions' => QuestionResource::collection($this->questions),
        ];
    }
}

<?php

namespace Belebo\Http\Resources\User\Provider;

use FileHelper;
use Illuminate\Http\Resources\Json\JsonResource;
use Belebo\Http\Resources\Admin\Location\LocationResource;

class ProfileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $emptyHours = array_fill(0, 4, '');
        return [
            'profile' => [
                'gender' => $this->gender,
                'first_name' => $this->first_name,
                'last_name' => $this->last_name,
                'phone' => $this->phone,
                'date_of_birth' => $this->date_of_birth->format('d/m/Y'),
                'email' => $this->email,
                'address' => $this->address,
                'location' => new LocationResource($this->location),
            ],
            'provider' => [
                'about' => $this->provider->about,
                'photo' => $this->provider->getFirstMediaUrl('photo'),
                'radius' => $this->provider->radius,
                'location' => new LocationResource($this->provider->location),
                'website' => $this->provider->website,
                'facebook' => $this->provider->facebook,
                'twitter' => $this->provider->twitter,
                'instagram' => $this->provider->instagram,
            ],
            'availability' => [
                'hours' => [
                    'd1' => $this->provider->availability->d1 ?? $emptyHours,
                    'd2' => $this->provider->availability->d2 ?? $emptyHours,
                    'd3' => $this->provider->availability->d3 ?? $emptyHours,
                    'd4' => $this->provider->availability->d4 ?? $emptyHours,
                    'd5' => $this->provider->availability->d5 ?? $emptyHours,
                    'd6' => $this->provider->availability->d6 ?? $emptyHours,
                    'd0' => $this->provider->availability->d0 ?? $emptyHours
                ],
                'from' => $this->provider->availability->formattedFrom,
                'to' => $this->provider->availability->formattedTo,
                'unavailable' => $this->provider->availability->unavailable
            ],
            'services' => $this->provider->services->pluck('id'),
            'company' => [
                'name' => $this->provider->company->name,
                'address' => $this->provider->company->address,
                'location' => new LocationResource($this->provider->company->location),
                'siren' => $this->provider->company->siren,
                'iban' => $this->provider->company->iban,
                'certificate' => FileHelper::toBase64($this->provider->company->getFirstMedia('certificate')->getPath())
            ],
            /*
            'representative' => new RepresentativeResource($this->provider->representative),
            'shareholders' => RepresentativeResource::collection($this->provider->shareholders)
            */
        ];
    }
}

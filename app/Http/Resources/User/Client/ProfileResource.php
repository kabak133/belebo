<?php

namespace Belebo\Http\Resources\User\Client;

use Belebo\Http\Resources\Location\LocationResource;
use Illuminate\Http\Resources\Json\Resource;

class ProfileResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'gender' => $this->gender,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'phone' => '+33'.$this->phone,
            'date_of_birth' => $this->date_of_birth->format('d/m/Y'),
            'email' => $this->email,
            'address' => $this->address,
            'location' => new LocationResource($this->location),
            'has_password' => $this->has_password,
            'addresses' => AddressResource::collection($this->addresses),
        ];
    }
}

<?php

namespace Belebo\Http\Resources\User\Client;

use Illuminate\Http\Resources\Json\Resource;
use Belebo\Http\Resources\Location\LocationResource;

class AddressResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'gender' => $this->gender,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'address' => $this->address,
            'location' => new LocationResource($this->location),
            'phone' => '+33'.$this->phone,
            'default' => $this->default
        ];
    }
}

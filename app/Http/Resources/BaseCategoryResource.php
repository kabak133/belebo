<?php

namespace Belebo\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class BaseCategoryResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            $this->mergeWhen($request->is('*/admin/*'), [
                'id' => $this->id
            ]),
            'name' => $this->name
        ];
    }
}

<?php

namespace Belebo\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class PromoCodeResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */

    public function toArray($request)
    {
        return [
                  'id' => $this->id,
                'name' => $this->name,
            'discount' => $this->discount,
              'active' => $this->active,
             'counter' => $this->counter,
                'from' => $this->from,
                  'to' => $this->to,
                'type' => $this->type
        ];
    }
}
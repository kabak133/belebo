<?php

namespace Belebo\Http\Resources\Service;

use Storage;
use Illuminate\Http\Resources\Json\Resource;

class ServiceResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $category = $request->is('*/' . $this->id) ? $this->category : $this->category->name;
        return [
            $this->mergeWhen(
                $request->is('*/services') || $request->is('*/admin/*'), [
                'id' => $this->id,
                'name' => $this->name,
                'details' => $this->details,
                'preparation' => $this->preparation,
                'trick' => $this->trick
            ]),
            'image' => $this->getFirstMediaUrl('image'),
            'price' => $this->price,
            'duration' => $this->duration,
            $this->mergeWhen(
                $request->is('*/admin/*'), [
                'created_at' => $this->created_at,
                'updated_at' => $this->updated_at,
                'category' => $category
            ])
        ];
    }
}

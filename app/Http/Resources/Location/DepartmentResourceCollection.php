<?php

namespace Belebo\Http\Resources\Location;

use Illuminate\Http\Resources\Json\ResourceCollection;

class DepartmentResourceCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => [
                'available' => $this->collection->where('available', true)->values(),
                'unavailable' => $this->collection->where('available', false)->values(),
            ]
        ];
    }
}

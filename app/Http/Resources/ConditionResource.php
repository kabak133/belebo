<?php

namespace Belebo\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ConditionResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'text'=>$this->text
        ];
    }
}

<?php

namespace Belebo\Http\Resources\News;

use Illuminate\Http\Resources\Json\Resource;

class NewsResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $image = $this->getFirstMedia('horizontal');
        $showVerticalImage = $request->query('first') || $request->is('*/' . $this->id);
        $showText = $request->is('*/' . $this->slug) || $showVerticalImage;
        return [
            'title' => $this->title,
            'slug' => $this->slug,
            $this->mergeWhen($showText, ['text' => $this->text]),
            'image' => [
                $this->mergeWhen(
                    $showText,
                    ['original' => $image->getUrl()]
                ),
                'sm' => $image->getUrl('thumb-sm'),
                'lg' => $image->getUrl('thumb-lg'),
                $this->mergeWhen(
                    $showVerticalImage,
                    ['vertical' => $this->getFirstMediaUrl('vertical', 'thumb')]
                ),
            ],
            'category' => new BaseCategoryResource($this->whenLoaded('category')),
            'tags' => BaseCategoryResource::collection($this->tags),
            'created_at' => $this->created_at,
            $this->mergeWhen($request->is('*/admin/*'), [
                'id' => $this->id,
                'published' => $this->published,
                'updated_at' => $this->updated_at
            ]),
        ];
    }
}

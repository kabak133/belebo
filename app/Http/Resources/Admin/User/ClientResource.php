<?php

namespace Belebo\Http\Resources\Admin\User;

use Illuminate\Http\Resources\Json\Resource;
use Belebo\Http\Resources\Location\LocationResource;

class ClientResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'gender' => $this->gender,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'phone' => $this->phone,
            'date_of_birth' => $this->date_of_birth->format('d/m/Y'),
            'email' => $this->email,
            'address' => $this->address,
            'active' => $this->active,
            'location' => new LocationResource($this->location),
        ];
    }
}

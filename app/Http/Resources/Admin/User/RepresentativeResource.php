<?php

namespace Belebo\Http\Resources\Admin\User;

use FileHelper;
use Illuminate\Http\Resources\Json\JsonResource;
use Belebo\Http\Resources\Admin\Location\LocationResource;

class RepresentativeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'gender' => $this->gender,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'date_of_birth' => $this->date_of_birth->format('d/m/Y'),
            'address' => $this->address,
            'location' => new LocationResource($this->location),
            'passport' => FileHelper::toBase64($this->getFirstMedia('passport')->getPath()),
        ];
    }
}

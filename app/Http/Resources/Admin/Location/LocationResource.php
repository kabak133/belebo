<?php

namespace Belebo\Http\Resources\Admin\Location;

use Illuminate\Http\Resources\Json\Resource;

class LocationResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'city' => new CityResource($this->city),
            'postcode' => new PostcodeResource($this->postcode),
        ];
    }
}

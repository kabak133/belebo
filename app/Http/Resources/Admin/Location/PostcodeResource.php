<?php

namespace Belebo\Http\Resources\Admin\Location;

use Illuminate\Http\Resources\Json\JsonResource;

class PostcodeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'code' => $this->code,
            'department' => new DepartmentResource($this->department),
        ];
    }
}

<?php

namespace Belebo\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\Resource;

class HolidayResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
              'id' => $this->id,
            'name' => $this->name,
            'date' => $this->date,
        ];
    }
}
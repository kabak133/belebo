<?php

namespace Belebo\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\TransformsRequest;

class RemoveNonDigitsFromPhone extends TransformsRequest
{
    /**
     * Transform the phone.
     *
     * @param string $key
     * @param mixed $value
     * @return mixed
     */
    protected function transform($key, $value)
    {
        return $key === 'phone' ? preg_replace('/\D/', '', $value) : $value;
    }
}

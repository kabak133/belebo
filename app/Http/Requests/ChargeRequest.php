<?php

namespace Belebo\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChargeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()){
            case 'POST':
                return [
                      'addressId' => ['required', 'bail', 'numeric', 'exists:locations__city_postcode,id'],
                    'stripeToken' => ['required', 'bail', 'string'],
                       'products' => ['required', 'array'],
                     'products.*' => ['required', 'numeric'],
                 'infoOrder.date' => ['required_without_all:infoOrder.present,infoOrder.moment', 'date_format:Y-m-d H:s'],
              'infoOrder.message' => ['sometimes', 'bail'],
               'infoOrder.moment' => ['required_without_all:infoOrder.date,infoOrder.present', 'boolean'],
              'infoOrder.present' => ['required_without_all:infoOrder.date,infoOrder.moment', 'boolean'],
                          'promo' => ['sometimes', 'bail', 'string'],
                ];
            case 'GET' :
                return [
                    'amount' => ['required', 'bail', 'integer']
                ];
        }

    }

    
}
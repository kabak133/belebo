<?php

namespace Belebo\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class FileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $type = $request->type;
        switch ($type) {
            case 'image':
                $rules = ['image'];
                break;
            default:
                $rules = ['file'];
                break;
        }
        return [
            'file' => array_merge(['bail', 'required', 'max:10240'], $rules)
        ];
    }
}

<?php

namespace Belebo\Http\Requests\User;

use Belebo\Rules\InTemp;
use Belebo\Models\User\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Belebo\Models\Location\Location;
use Belebo\Models\Location\Department;
use Illuminate\Foundation\Http\FormRequest;

class ProviderRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @param Request $request
     * @return array
     */
    public function rules(Request $request)
    {
        $rules = [
            'provider.gender' => ['bail', 'required', 'in:male,female'],
            'provider.first_name' => ['bail', 'required', 'string'],
            'provider.last_name' => ['bail', 'required', 'string'],
            'provider.date_of_birth' => ['bail', 'required', 'date_format:d/m/Y'],
            'provider.email' => ['bail', 'required', 'email', Rule::unique(User::getTableName(), 'email')],
            'provider.phone' => ['bail', 'required', 'string', 'size:9'],
            'provider.address' => ['bail', 'required', 'string'],
            'provider.location_id' => ['bail', 'required', 'exists:' . Location::getTableName() . ',id'],
            'provider.passport' => ['bail', 'required', new InTemp],

            'company.name' => ['bail', 'required', 'string'],
            'company.address' => ['bail', 'required', 'string'],
            'company.location_id' => ['bail', 'required', 'exists:' . Location::getTableName() . ',id'],
            'company.siren' => ['bail', 'required', 'string', 'max:9'],
            'company.iban' => ['bail', 'required', 'string', 'size:27'],
            'company.certificate' => ['bail', 'required', new InTemp],
            'company.departments' => ['bail', 'required', 'array'],
            'company.departments.*' => ['bail', 'exists:' . Department::getTableName() . ',id'],

            'representative.gender' => ['bail', 'required', 'in:male,female'],
            'representative.first_name' => ['bail', 'required', 'string'],
            'representative.last_name' => ['bail', 'required', 'string'],
            'representative.date_of_birth' => ['bail', 'required', 'date_format:d/m/Y'],
            'representative.address' => ['bail', 'required', 'string'],
            'representative.location_id' => ['bail', 'required', 'exists:' . Location::getTableName() . ',id'],
            'representative.passport' => ['bail', 'required', new InTemp],

            'shareholders' => ['bail', 'array', 'between:0,4'],
        ];

        if (!empty($request->shareholders)) {
            $rules = array_merge($rules, [
                'shareholders.*.gender' => ['bail', 'required', 'in:male,female'],
                'shareholders.*.first_name' => ['bail', 'required', 'string'],
                'shareholders.*.last_name' => ['bail', 'required', 'string'],
                'shareholders.*.date_of_birth' => ['bail', 'required', 'date_format:d/m/Y'],
                'shareholders.*.address' => ['bail', 'required', 'string'],
                'shareholders.*.location_id' => ['bail', 'required', 'exists:' . Location::getTableName() . ',id'],
                'shareholders.*.passport' => ['bail', 'required', new InTemp]
            ]);
        }
        return $rules;
    }
}

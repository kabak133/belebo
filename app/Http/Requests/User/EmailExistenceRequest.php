<?php

namespace Belebo\Http\Requests\User;

use Belebo\Models\User\User;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class EmailExistenceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->id === 'me') {
            $id = auth('api')->user()->id;
        } else {
            $id = $this->id;
        }

        return [
            'email' => ['bail', 'required', 'email', Rule::unique(User::getTableName())->ignore($id ?? NULL)]
        ];
    }
}

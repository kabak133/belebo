<?php

namespace Belebo\Http\Requests\User;

use Auth;
use Illuminate\Http\Request;
use Belebo\Models\User\User;
use Illuminate\Validation\Rule;
use Belebo\Models\Location\Location;
use Illuminate\Foundation\Http\FormRequest;

class ClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @param Request $request
     * @return array
     */
    public function rules(Request $request)
    {
        $user = $request->is('*/profile') ? Auth::user() : $this->user;
        $required = $user ? ['bail'] : ['bail', 'required'];

        $email = ['email'];
        $password = ['confirmed', 'string', 'min:6'];
        if ($request->is('*/register/client')) { //if it's registration action
            $password = array_merge(['required_without:token'], $password);
            $token = ['required_without:password', 'string'];
            $email = array_merge($email, ['confirmed']);
        } elseif ($request->is('*/profile')) { //if it's updating profile action
            $password = $token = [];
        } else { //if it's admin action
            $password = array_merge($required, $password);
            $token = [];
        }

        return [
            'gender' => array_merge($required, ['in:male,female']),
            'first_name' => array_merge($required, ['string']),
            'last_name' => array_merge($required, ['string']),
            'email' => array_merge($required, $email, [Rule::unique(User::getTableName())->ignore($user->id ?? NULL)]),
            'phone' => array_merge($required, ['string','numeric','size:9']),
            'date_of_birth' => array_merge($required, ['date_format:d/m/Y']),
            'address' => array_merge($required, ['string']),
            'location_id' => array_merge($required, ['exists:' . Location::getTableName() . ',id']),
            'password' => $password,
            'token' => $token
        ];
    }

    // /**
    //  * Get the error messages for the defined validation rules.
    //  *
    //  * @return array
    //  */
    // public function messages()
    // {
    //     return [
    //         'addressID.unique:locations__city_postcode, id' => 'Actuellement, le service ne fonctionne que dans les villes, deux départements: le Bas-Rhin et le Haut-Rhin.'
    //     ];
    // }
}

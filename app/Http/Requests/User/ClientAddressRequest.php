<?php

namespace Belebo\Http\Requests\User;

use Belebo\Models\Location\Location;
use Illuminate\Foundation\Http\FormRequest;

class ClientAddressRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $required = $this->address ? ['bail'] : ['bail', 'required'];
        return [
            'name' => array_merge($required, ['string']),
            'gender' => array_merge($required, ['in:male,female']),
            'first_name' => array_merge($required, ['string']),
            'last_name' => array_merge($required, ['string']),
            'phone' => array_merge($required, ['string', 'size:9']),
            'address' => array_merge($required, ['string']),
            'location_id' => array_merge($required, ['exists:' . Location::getTableName() . ',id']),
            'default' => ['boolean']
        ];
    }
}

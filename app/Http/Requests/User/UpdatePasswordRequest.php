<?php

namespace Belebo\Http\Requests\User;

use Auth;
use Illuminate\Foundation\Http\FormRequest;

class UpdatePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $required = Auth::user()->hasPassword ? ['required'] : [];
        return [
            'password' => array_merge($required, ['string', 'min:6']),
            'password_new' => ['required', 'string', 'min:6']
        ];
    }
}

<?php

namespace Belebo\Http\Requests\User;

use Auth;
use Belebo\Models\User\User;
use Illuminate\Validation\Rule;
use Belebo\Models\Location\Location;
use Illuminate\Foundation\Http\FormRequest;

class ProviderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'profile.gender' => ['bail', 'in:male,female'],
            'profile.first_name' => ['bail', 'string'],
            'profile.last_name' => ['bail', 'string'],
            'profile.date_of_birth' => ['bail', 'date_format:d/m/Y'],
            'profile.email' => ['bail', 'email', Rule::unique(User::getTableName(), 'email')->ignore($this->user->id ?? Auth::user()->id)],
            'profile.phone' => ['bail', 'string', 'size:9'],
            'profile.address' => ['bail', 'string'],
            'profile.location_id' => ['bail', 'exists:' . Location::getTableName() . ',id'],

            'photo' => ['bail', 'image'],

            'services' => ['bail', 'array'],
            'active' => ['bail', 'boolean'],

            'company.name' => ['bail', 'string'],
            'company.address' => ['bail', 'string'],
            'company.location_id' => ['bail', 'exists:' . Location::getTableName() . ',id'],
            'company.siren' => ['bail', 'string', 'max:9'],
            'company.iban' => ['bail', 'string', 'size:27'],
        ];
    }
}

<?php

namespace Belebo\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WorkshopRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => ['bail','required','string'],
            'last_name' => ['bail','required','string'],
            'email' => ['bail','required','email'],
            'phone' => ['bail', 'required','string', 'size:9'],
            'datetime' => ['bail', 'required'],
            'participants' => ['bail', 'required', 'numeric', 'between:2,6'],
            'address' => ['bail', 'required','string'],
            'message' => ['bail', 'required'],
        ];
    }
}

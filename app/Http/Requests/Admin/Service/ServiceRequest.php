<?php

namespace Belebo\Http\Requests\Admin\Service;

use Belebo\Rules\InTemp;
use Belebo\Models\Service\Category;
use Illuminate\Foundation\Http\FormRequest;

class ServiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->service) {
            $required = ['bail'];
            $image = ['bail', 'image', 'max:10240'];
        } else {
            $required = ['bail', 'required'];
            $image = ['bail', 'required', 'string', new InTemp];
        }
        return [
            'name' => array_merge($required, ['string']),
            'category_id' => array_merge($required, ['exists:' . Category::getTableName() . ',id']),
            'price' => array_merge($required, ['numeric', 'min:1']),
            'duration' => array_merge($required, ['numeric', 'min:1']),
            'details' => $required,
            'preparation' => $required,
            'trick' => $required,
            'image' => $image,
        ];
    }
}

<?php

namespace Belebo\Http\Requests\Admin\Service;

use Illuminate\Validation\Rule;
use Belebo\Models\Service\Category;
use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $required = $this->category ? [] : ['required'];
        return [
            'name' => array_merge($required, ['bail', 'string', Rule::unique(Category::getTableName())->ignore($this->category->id ?? NULL)]),
        ];
    }
}

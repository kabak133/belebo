<?php

namespace Belebo\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class HolidayRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $name = ['required', 'string', 'unique:holidays,name'];

        if($this->method() == 'PUT'){
            $holiday_id = $this->route()->parameter('holiday');
            $name = ['required','string', 'unique:holidays,name,'.$holiday_id];
        }

        return [
            'name' => $name,
            'date' => ['required', 'date_format:Y-m-d']
        ];
    }
}

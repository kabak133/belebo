<?php

namespace Belebo\Http\Requests\Admin\Question;

use Belebo\Models\Question\Category;
use Illuminate\Foundation\Http\FormRequest;

class QuestionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $required = $this->question ? ['bail'] : ['bail', 'required'];
        return [
            'question' => array_merge($required, ['string']),
            'answer' => $required,
            'category_id' => array_merge($required, ['exists:' . Category::getTableName() . ',id']),
        ];
    }
}

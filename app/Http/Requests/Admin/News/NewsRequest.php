<?php

namespace Belebo\Http\Requests\Admin\News;

use Belebo\Rules\InTemp;
use Belebo\Models\News\{
    Category, Tag
};
use Illuminate\Foundation\Http\FormRequest;

class NewsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->news) {
            $required = ['bail'];
            $images = [
                'image' => ['bail', 'image', 'max:10240'],
                'image_v' => ['bail', 'image', 'max:10240']
            ];
        } else {
            $required = ['bail', 'required'];
            $images = [
                'image' => ['bail', 'required', 'string', new InTemp],
                'image_v' => ['bail', 'string', new InTemp]
            ];
        }
        return array_merge($images,
            [
                'title' => array_merge($required, ['string']),
                'slug' => ['bail', 'string'],
                'text' => $required,
                'category_id' => array_merge($required, ['exists:' . Category::getTableName() . ',id']),
                'published' => ['bail', 'boolean'],
                'tags' => ['bail', 'array'],
                'tags.*' => ['bail', 'exists:' . Tag::getTableName() . ',id']
            ]);
    }
}

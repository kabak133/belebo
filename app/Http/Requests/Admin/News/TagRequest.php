<?php

namespace Belebo\Http\Requests\Admin\News;

use Illuminate\Validation\Rule;
use Belebo\Models\News\Tag;
use Illuminate\Foundation\Http\FormRequest;

class TagRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $required = $this->tag ? [] : ['required'];
        return [
            'name' => array_merge($required, ['bail', 'string', Rule::unique(Tag::getTableName())->ignore($this->tag->id ?? NULL)]),
            'slug' => ['bail', 'string'],
        ];
    }
}

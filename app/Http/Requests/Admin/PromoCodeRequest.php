<?php

namespace Belebo\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PromoCodeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $name = ['required','string', 'unique:promocodes,name'];

        if($this->method() == 'PATCH'){
            $promo_id = $this->route('promocode');
            $name = ['required','string', Rule::unique('promocodes')->ignore($promo_id)];
        }

        return [
                'name' => $name,
            'discount' => ['required', 'numeric'],
              'active' => ['required', 'boolean'],
             'type_id' => ['required', 'numeric'],
             'counter' => ['sometimes', 'required', 'numeric'],
                'from' => ['sometimes', 'required', 'date_format:Y-m-d'],
                  'to' => ['sometimes', 'required', 'date_format:Y-m-d']
        ];
    }
}

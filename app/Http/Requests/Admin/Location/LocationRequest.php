<?php

namespace Belebo\Http\Requests\Admin\Location;

use Belebo\Models\Location\{
    City, Postcode, Department
};
use Belebo\Rules\UniqueLocation;
use Illuminate\Foundation\Http\FormRequest;

class LocationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->location) {
            $city = $postcode = $department_id = [];
            $city_id = [new UniqueLocation(null, $this->postcode_id, $this->location->id)];
            $postcode_id = [new UniqueLocation($this->city_id, null, $this->location->id)];
        } else {
            $city = ['required_without:city_id'];
            $city_id = [new UniqueLocation(null, $this->postcode_id), 'required_without:city'];
            $postcode_id = [new UniqueLocation($this->city_id), 'required_without:postcode'];
            $department_id = $postcode = ['required_without:postcode_id'];
        }

        return [
            'city' => array_merge($city, ['bail', 'string']),
            'city_id' => array_merge($city_id, ['bail', 'exists:' . City::getTableName() . ',id']),
            'postcode' => array_merge($postcode, ['bail', 'numeric']),
            'postcode_id' => array_merge($postcode_id, ['bail', 'exists:' . Postcode::getTableName() . ',id']),
            'department_id' => array_merge($department_id, ['bail', 'exists:' . Department::getTableName() . ',id']),
        ];
    }
}

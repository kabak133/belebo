<?php

namespace Belebo\Traits;

trait TableNameGetter
{
    public static function getTableName()
    {
        return ((new self)->getTable());
    }
}

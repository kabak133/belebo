<?php

namespace Belebo\Traits;

use Belebo\Models\Location\Location;

trait HasLocation
{
    /**
     * Get Location
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function location()
    {
        return $this->belongsTo(Location::class);
    }

    /**
     * Get City
     *
     * @return \Belebo\Models\Location\City
     */
    public function getCityAttribute()
    {
        return $this->location->city->name;
    }

    /**
     * Get Postcode
     *
     * @return \Belebo\Models\Location\Postcode
     */
    public function getPostcodeAttribute()
    {
        return $this->location->postcode->code;
    }
}

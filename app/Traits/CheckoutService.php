<?php

namespace Belebo\Traits;

trait CheckoutService
{
    public function getServicesId(array $cart)
    {
        $services_id = [];

        foreach ($cart as $service){
            $services_id[] = $service->id;
        }

        return $services_id;
    }
}
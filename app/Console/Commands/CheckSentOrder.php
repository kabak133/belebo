<?php

namespace Belebo\Console\Commands;

use Belebo\Models\Order;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class CheckSentOrder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check_status:order';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check if order status sent more then 15 minutes - delete order';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $orders = Order::query()
            ->where('status', 'sent')
            ->whereNotBetween('created_at',[
                Carbon::now()->subMinutes(20)->format('Y-m-d H:s'),

                Carbon::now()->format('Y-m-d H:s')
            ])
            ->with('services')
            ->get();

        if(count($orders)){
            foreach ($orders as $order){
                $order->services()->detach();
            }

            if(!Order::destroy($orders->pluck('id')->toArray())){
                Log::alert('Orders failed to destroy');
            } else {
                Log::info('Delete all unanswered orders');
            };

        }

    }
}

<?php

namespace Belebo\Mail;

use Belebo\Models\Feedback;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class FeedbackMail extends Mailable
{
    use Queueable, SerializesModels;

    public $feedback;

    /**
     * Create a new message instance.
     *
     * FeedbackMail constructor.
     * @param Feedback $feedback
     */
    public function __construct(Feedback $feedback)
    {
        $this->feedback = $feedback;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(env('APP_EMAIL', 'hello@leschouchoutes.com'), env('APP_NAME', 'Leschouchoutes'))
            ->to(env('APP_EMAIL', 'hello@leschouchoutes.com'))
            ->view('emails.feedback')
            ->text('emails.feedback_plain');
    }
}

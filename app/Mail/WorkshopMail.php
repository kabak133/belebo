<?php

namespace Belebo\Mail;

use Belebo\Models\Workshop;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class WorkshopMail extends Mailable
{
    use Queueable, SerializesModels;

    public $workshop;

    /**
     * Create a new message instance.
     *
     * WorkshopMail constructor.
     * @param Workshop $workshop
     */
    public function __construct(Workshop $workshop)
    {
        $this->workshop = $workshop;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(env('APP_EMAIL', 'hello@leschouchoutes.com'), env('APP_NAME', 'Leschouchoutes'))
            ->to(env('APP_EMAIL', 'hello@leschouchoutes.com'))
            ->view('emails.workshop')
            ->text('emails.workshop_plain');
    }
}

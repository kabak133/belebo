<?php

return [

    // Discount for services
//    'discount' => [
//        2 => [
//            'percent' => 5,
//        ],
//        3 => [
//            'percent' => 10,
//        ],
//        4 => [
//            'percent' => 15,
//        ]
//    ],

    'discount' => [
        [
            'percent' => 5,
            'count' => 2
        ],
        [
            'percent' => 10,
            'count' => 3
        ],
        [
            'percent' => 15,
            'count' => 4
        ],
    ],

    'holidays_charge' => 15,

    'order_status' => [
       1 => 'sent',
       2 => 'accept',
       3 => 'done'
    ],

    'messages' => [
        'user' => [

        ],
        'provider' => [

        ]
    ]
];
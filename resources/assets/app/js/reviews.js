let reviews = [

  {
    id:1,
    rating: 5,
    text: `Jeune maman, je n’ai jamais le temps d’aller au salon. 
      j’ai profité de la sieste du petit pour commander un chouchouteur: 
      en 15 minutes le coiffeur était chez moi ! Pour couronner le tout, 
      j’adore ma nouvelle coupe !`,
    author: 'JESSICA, Strasbourg'
  },
  {
    id:2,
    rating: 4,
    text: `Super coupe et service ! Dommage que  je doive aller chez ma tante
     pour avoir accès aux chouchoutés... Ils ne sont pas encore disponibles dans ma région!`,
    author: 'Éloise, Lyon',
    answer: `La réponse des chouchoutés
      Bonjour Éloise, merci de votre retour. Nous recrutons actuellement des chouchouteurs dans de nouvelles zones 
      géographiques. Nos services seront peut être bientôt présent chez vous !`,
  },
  {
    id:3,
    rating: 5,
    text: `Jeune maman, je n’ai jamais le temps d’aller au salon. 
      j’ai profité de la sieste du petit pour commander un chouchouteur: 
      en 15 minutes le coiffeur était chez moi ! Pour couronner le tout, 
      j’adore ma nouvelle coupe !`,
    author: 'JESSICA, Strasbourg',
    answer: `La réponse des chouchoutés
      Bonjour Éloise, merci de votre retour. Nous recrutons actuellement des chouchouteurs dans de nouvelles zones 
      géographiques. Nos services seront peut être bientôt présent chez vous !`,
  },
  {
    id:4,
    rating: 4,
    text: `Jeune maman, je n’ai jamais le temps d’aller au salon. 
      j’ai profité de la sieste du petit pour commander un chouchouteur: 
      en 15 minutes le coiffeur était chez moi ! Pour couronner le tout, 
      j’adore ma nouvelle coupe !`,
    author: 'JESSICA, Strasbourg'
  },
  {
    id:5,
    rating: 5,
    text: `Jeune maman, je n’ai jamais le temps d’aller au salon. 
      j’ai profité de la sieste du petit pour commander un chouchouteur: 
      en 15 minutes le coiffeur était chez moi ! Pour couronner le tout, 
      j’adore ma nouvelle coupe !`,
    author: 'JESSICA, Strasbourg'
  },
  {
    id:6,
    rating: 4,
    text: `Jeune maman, je n’ai jamais le temps d’aller au salon. 
      j’ai profité de la sieste du petit pour commander un chouchouteur: 
      en 15 minutes le coiffeur était chez moi ! Pour couronner le tout, 
      j’adore ma nouvelle coupe !`,
    author: 'JESSICA, Strasbourg'
  },
  {
    id:7,
    rating: 5,
    text: `Jeune maman, je n’ai jamais le temps d’aller au salon. 
      j’ai profité de la sieste du petit pour commander un chouchouteur: 
      en 15 minutes le coiffeur était chez moi! Pour couronner le tout, 
      j’adore ma nouvelle coupe !`,
    author: 'JESSICA, Strasbourg'
  },
  {
    id:8,
    rating: 3,
    text: `Jeune maman, je n’ai jamais le temps d’aller au salon. 
      j’ai profité de la sieste du petit pour commander un chouchouteur: 
      en 15 minutes le coiffeur était chez moi ! Pour couronner le tout, 
      j’adore ma nouvelle coupe !`,
    author: 'JESSICA, Strasbourg',
    answer: `La réponse des chouchoutés
      Bonjour Éloise, merci de votre retour. Nous recrutons actuellement des chouchouteurs dans de nouvelles zones 
      géographiques. Nos services seront peut être bientôt présent chez vous !`,
  },
  {
    id:9,
    rating: 5,
    text: `Jeune maman, je n’ai jamais le temps d’aller au salon. 
      j’ai profité de la sieste du petit pour commander un chouchouteur: 
      en 15 minutes le coiffeur était chez moi ! Pour couronner le tout, 
      j’adore ma nouvelle coupe !`,
    author: 'JESSICA, Strasbourg'
  }

];

export default reviews;
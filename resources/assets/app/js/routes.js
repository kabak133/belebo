import VueRouter from 'vue-router';
import Home from './views/HomePage';
import AuthCallback from './views/AuthCallbackPage';
import ProviderRegistration from './views/ProviderRegistrationPage';
import Profile from './views/ProfilePage';
import Services from './views/ServicesPage';
import Workshops from './views/WorkshopsPage';
import Marriage from './views/MarriagePage';
import NewsList from './views/NewsListPage';
import NewsItem from './views/NewsItemPage';
import Questions from './views/QuestionsPage.vue';
import Contact from './views/ContactPage.vue';
import Conditions from './views/ConditionsPage.vue';
import Checkout from './views/CheckoutPage';
import CheckoutInfo from './views/CheckoutInfoPage';
import CheckoutSelection from './views/CheckoutSelectionPage';
import Confirmation from './views/CheckoutConfirmationPage';
import ReviewsPage from './views/ReviewsPage'
import FeedbackPage from './views/FeedbackPage'

let routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/oauth/:provider/callback',
    name: 'oauth.callback',
    component: AuthCallback
  },
  {
    path: '/register',
    name: 'register.provider',
    component: ProviderRegistration
  },
  {
    path: '/profile',
    name: 'profile',
    component: Profile
  },
  {
    path: '/services',
    name: 'services',
    component: Services
  },
  {
    path: '/workshops',
    name: 'workshops',
    component: Workshops
  },
  {
    path: '/marriage',
    name: 'marriage',
    component: Marriage
  },
  {
    path: '/news',
    name: 'news',
    component: NewsList,
    children: [
      {
        path: 'category/:slug',
        name: 'news.category',
        component: NewsList
      },
      {
        path: 'tag/:slug',
        name: 'news.tag',
        component: NewsList
      }
    ],
  },
  {
    path: '/news/:slug?',
    name: 'news.show',
    component: NewsItem
  },
  {
    path: '/questions',
    name: 'questions',
    component: Questions
  },
  {
    path: '/contacts',
    name: 'contact',
    component: Contact
  },
  {
    path: '/conditions',
    name: 'conditions',
    component: Conditions
  },
  {
    path: '/checkout',
    /*name: 'checkout',*/
    component: Checkout,
    children: [
      {
        path: '',
        name: 'selection',
        component: CheckoutSelection,
        meta: {}
      },
      {
        path: 'information',
        name: 'information',
        component: CheckoutInfo
      },
      {
        path: 'confirmation',
        name: 'confirmation',
        component: Confirmation
      }
    ]
  },
  {
    path: '/reviews',
    name: 'reviews',
    component: ReviewsPage
  },
  {
    path: '/feedback',
    name: 'feedback',
    component: FeedbackPage
  }
];

export default new VueRouter({
  routes,
  linkActiveClass: 'active',
  linkExactActiveClass: 'exact-active',
  mode: 'history',
  scrollBehavior(to, from, savedPosition) {
    return {x: 0, y: 0}
  }
});

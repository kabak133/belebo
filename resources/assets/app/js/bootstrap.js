import Vue from 'vue';
import axios from 'axios';
import moment from 'moment';
import lodash from 'lodash';
import params from './params';
import VueCookie from 'vue-cookie';
import VeeValidate, {Validator} from 'vee-validate';
import BootstrapVue from 'bootstrap-vue';
import './prototypes/string';
import fr from 'vee-validate/dist/locale/fr';
import Vue2Filters from 'vue2-filters'

import DatePicker from 'vue2-datepicker'
// import rate from 'vue-rate';
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { far } from '@fortawesome/free-regular-svg-icons'

import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'


library.add(fas)
library.add(far)

Vue.component('font-awesome-icon', FontAwesomeIcon)

Validator.localize('fr', fr);

window._ = lodash;

moment.locale('fr');
window.moment = moment;

axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
window.axios = axios;

Vue.prototype.$http = axios;
Vue.prototype.$moment = moment;
Vue.prototype.$params = params;

Vue.use(VueCookie)
  .use(VeeValidate)
  .use(BootstrapVue)
  .use(Vue2Filters)
  .use(DatePicker)


window.Vue = Vue;

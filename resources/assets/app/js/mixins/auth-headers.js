export default {
    computed: {
        AuthHeaders() {
            return {
                headers: {
                    Authorization: 'Bearer ' + this.$cookie.get('token')
                }
            }
        }
    },
}
import Bus from '../bus';
import AuthHeaders from './auth-headers';

export default {
  mixins: [AuthHeaders],
  data() {
    return {
      auth: false,
    };
  },
  methods: {
    checkAuth() {

      if (this.$cookie.get('token')) {

        this.$http.get(
            route('auth.check'),
            this.AuthHeaders,
        ).then((r) => {
          console.log(r)
          this.authorize();
        }).catch(() => {
          this.$cookie.delete('token');
        });
      }
    },
    authorize() {
      Bus.$emit('auth', true);
    },
    deauthorize(redirect = false, force = false) {
      Bus.$emit('auth', false);
      this.$http.post(
          route('logout'),
          null,
          this.AuthHeaders,
      );
      this.$cookie.delete('token');
      if (redirect) {
        if (force) {
          window.location.href = '/';
        }

        this.$router.push('/');
      }
    },
    showAuthModal() {
      this.$root.$emit('bv::show::modal', 'auth-modal');
    },
    hideAuthModal() {
      this.$root.$emit('bv::hide::modal', 'auth-modal');
    },
  },
  beforeMount() {
    Bus.$on('auth', (auth) => {
      this.auth = auth;
    });
  },
};

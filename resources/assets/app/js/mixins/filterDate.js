import moment from 'moment'
export default {
    filters: {
        filterDate: function(string, format) {
            if( string ){
                let newDate
                newDate = moment(string).format(format);
                return newDate;
            }

        }
    }
}

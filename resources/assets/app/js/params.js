let params = {
    "contact": {
        "email": "hello@leschouchoutes.com",
        "phone": "0367360070"
    },
    "social": {
        "facebook": "https://www.facebook.com/",
        "instagram": "https://www.instagram.com/",
        "pinterest": "https://www.pinterest.com/",
        "twitter": "https://twitter.com/",
    },
    "stripe": {
        "key": ""
    }
};

export default params;

import '../../app/js/bootstrap';
import router from './routes';
import App from './App.vue'

const app = new Vue({
    el: '#app',
    router,
    template: '<App/>',
    components: {App}
});

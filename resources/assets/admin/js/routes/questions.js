import List from '../views/Questions/QuestionsListPage';
import Categories from '../views/Questions/QuestionsCategoriesPage';

let routes = [
    {
        path: '/admin/questions',
        name: 'questions',
        component: List,
    },
    {
        path: '/admin/questions/categories',
        name: 'questions.categories',
        component: Categories
    }
];

export default routes;
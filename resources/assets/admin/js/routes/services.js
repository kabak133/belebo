import Categories from '../views/Services/CategoriesPage';
import Services from '../views/Services/ServicesPage';
import CreateService from '../views/Services/ServiceCreatePage';
import UpdateService from '../views/Services/ServiceUpdatePage';

let routes=[
    {
        path: '/admin/services/categories',
        name: 'services.categories',
        component: Categories,
    },
    {
        path: '/admin/services',
        name: 'services',
        component: Services,
    },
    {
        path: '/admin/services/create',
        name: 'services.create',
        component: CreateService
    },
    {
        path: '/admin/services/:id',
        name: 'services.edit',
        component: UpdateService
    },
];

export default routes;

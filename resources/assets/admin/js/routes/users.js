import ClientsList from '../views/Users/UsersClientsListPage';
import ClientsCreate from '../views/Users/UsersClientsCreateItemPage';
import ClientsUpdate from '../views/Users/UsersClientsUpdateItemPage';
import ProvidersList from '../views/Users/UsersProvidersListPage';
import ProvidersCreate from '../views/Users/UsersProvidersCreateItemPage';
import ProvidersUpdate from '../views/Users/UsersProvidersUpdateItemPage';
import AdminsList from '../views/Users/UsersAdminsListPage';
import AdminsCreate from '../views/Users/UsersAdminsCreateItemPage';
import AdminsUpdate from '../views/Users/UsersAdminsUpdateItemPage';

let routes = [
    {
        path: '/admin/users/clients',
        name: 'clients',
        component: ClientsList,
    },
    {
        path: '/admin/users/clients/create',
        name: 'clients.create',
        component: ClientsCreate,
    },
    {
        path: '/admin/users/clients/:id',
        name: 'clients.edit',
        component: ClientsUpdate,
    },
    {
        path: '/admin/users/providers',
        name: 'providers',
        component: ProvidersList,
    },
    {
        path: '/admin/users/providers/create',
        name: 'providers.create',
        component: ProvidersCreate,
    },
    {
        path: '/admin/users/providers/:id',
        name: 'providers.edit',
        component: ProvidersUpdate,
    },
    {
        path: '/admin/users/administrators',
        name: 'admins',
        component: AdminsList,
    },
    {
        path: '/admin/users/administrators/create',
        name: 'admins.create',
        component: AdminsCreate,
    },
    {
        path: '/admin/users/administrators/:id',
        name: 'admins.edit',
        component: AdminsUpdate,
    },
];

export default routes;

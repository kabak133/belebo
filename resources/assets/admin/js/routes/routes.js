import VueRouter from 'vue-router';
import Dashboard from '../views/DashboardPage';
import Workshops from '../views/WorkshopsPage';
import Feedbacks from '../views/FeedbacksPage';
import locations from './locations';
import services from './services';
import users from './users';
import news from './news';
import questions from './questions';
import conditions from './conditions';

import holidays from '../views/Discounts/Holidays'
import promocode from '../views/Discounts/Promocode'

let routes = [
    {
        path: '/admin',
        name: 'dashboard',
        component: Dashboard
    },
    {
        path: '/admin/workshops',
        name: 'workshops',
        component: Workshops
    },
    {
        path: '/admin/feedbacks',
        name: 'feedbacks',
        component: Feedbacks
    },
    {
        path: '/admin/holidays',
        name: 'holidays',
        component: holidays
    },
    {
        path: '/admin/promo',
        name: 'promo',
        component: promocode
    }
];

routes = routes.concat(locations);
routes = routes.concat(services);
routes = routes.concat(users);
routes = routes.concat(news);
routes = routes.concat(questions);
routes = routes.concat(conditions);

export default new VueRouter({
    routes,
    linkActiveClass: 'active',
    linkExactActiveClass: 'exact-active',
    mode: 'history',
    scrollBehavior(to, from, savedPosition) {
        return {x: 0, y: 0}
    }
});

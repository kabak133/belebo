import List from '../views/News/NewsListPage';
import ItemCreate from '../views/News/NewsItemCreatePage';
import ItemUpdate from '../views/News/NewsItemUpdatePage';
import Categories from '../views/News/NewsCategoriesPage';
import Tags from '../views/News/NewsTagsPage';

let routes = [
    {
        path: '/admin/news',
        name: 'news',
        component: List,
    },
    {
        path: '/admin/news/categories',
        name: 'news.categories',
        component: Categories
    },
    {
        path: '/admin/news/tags',
        name: 'news.tags',
        component: Tags
    },
    {
        path: '/admin/news/create',
        name: 'news.create',
        component: ItemCreate
    },
    {
        path: '/admin/news/:id',
        name: 'news.edit',
        component: ItemUpdate
    },
];

export default routes;

import General from '../views/Conditions/ConditionsGeneralPage';
import Provider from '../views/Conditions/ConditionsProviderPage';
import Client from '../views/Conditions/ConditionsClientPage';

let routes = [
    {
        path: '/admin/conditions/general',
        name: 'conditions.general',
        component: General,
    },
    {
        path: '/admin/conditions/provider',
        name: 'conditions.provider',
        component: Provider,
    },
    {
        path: '/admin/conditions/client',
        name: 'conditions.client',
        component: Client,
    }
];

export default routes;

import Locations from '../views/Location/LocationsPage';
import Departments from '../views/Location/DepartmentsPage';

let routes = [
    {
        path: '/admin/locations',
        name: 'locations',
        component: Locations
    },
    {
        path: '/admin/locations/departments',
        name: 'locations.departments',
        component: Departments
    }
];

export default routes;

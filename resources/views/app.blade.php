<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <script src="https://js.stripe.com/v3/"></script>


    <title>Leschouchoutes</title>
    <link href="{{asset('css/'.(Request::is('admin*')?'admin':'app').'.css')}}" rel="stylesheet" type="text/css">
</head>
<body>
<div id="app"></div>
@routes
<script src={{asset('js/'.(Request::is('admin*')?'admin':'app').'.js')}}></script>
</body>
</html>

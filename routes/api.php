 <?php

Route::namespace('User')->group(function () {
    Route::post('register/client', 'Client\RegisterController')->name('register.client');
    Route::post('register/provider', 'Provider\RegisterController')->name('register.provider');
    Route::post('exists', 'EmailExistenceController')->name('email.exists');
    Route::namespace('Auth')->group(function () {
        Route::post('login', 'LoginController@login')->name('login');
        Route::post('logout', 'LoginController@logout')->middleware(['auth:api'])->name('logout');
        Route::get('auth', 'LoginController@checkAuth')->middleware(['auth:api'])->name('auth.check');
        Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
        Route::post('password/reset', 'ResetPasswordController@reset')->name('password.reset');
        Route::get('oauth/urls', 'LoginController@getProvidersUrls')->name('oauth.urls');
        Route::get('oauth/{service}/url', 'LoginController@getProviderUrl')->name('oauth.url');
        Route::get('oauth/{service}/callback', 'LoginController@handleProviderCallback')->name('oauth.callback');
    });
    Route::middleware(['auth:api'])->group(function () {
        Route::prefix('profile')->as('profile.')->group(function () {
            Route::get('/', 'ProfileController@show')->name('show');
            Route::patch('/', 'ProfileController@update')->name('update');
            Route::namespace('Client')->middleware('client')->group(function () {
                Route::apiResource('/addresses', 'AddressController');
            });
        });
        Route::post('password/update', 'UpdatePasswordController')->name('password.update');
    });
});

Route::namespace('Location')->prefix('location')->as('location')->group(function () {
    Route::get('departments', 'DepartmentController')->name('.departments');
    Route::get('{location}', 'LocationController');
});

Route::apiResource('services', 'ServiceController', ['only' => ['index', 'show']]);

Route::namespace('News')->group(function () {
    Route::prefix('news')->as('news.')->group(function () {
        Route::apiResource('category', 'CategoryController', ['only' => ['index', 'show']]);
        Route::get('tag/{tag}', 'TagController')->name('tag.show');
    });
    Route::apiResource('news', 'NewsController', ['only' => ['index', 'show']]);
});

Route::post('file/{type?}', 'FileController')->name('file.store');
Route::get('questions', 'QuestionController')->name('questions');
Route::post('workshop', 'WorkshopController')->name('workshop');
Route::post('feedback', 'FeedbackController')->name('feedback');
Route::get('conditions/{condition}', 'ConditionController')->name('conditions');


//Route::middleware(['auth:api'])->group(function () {
    Route::prefix('checkout')->as('checkout.')->group(function (){
        Route::get('verify', 'CheckoutController@verifyPromo')->name('verify');
        Route::post('list', 'CheckoutController@showList')->name('list');
        Route::get('show', 'CheckoutController@showCheckout')->name('show');
        Route::get('date', 'CheckoutController@dateCheckout')->name('date');

        Route::middleware(['auth:api'])->group(function () {
            Route::post('charge', 'CheckoutController@charge')->name('charge');
        });

    });
//});

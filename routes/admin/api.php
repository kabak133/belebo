<?php

Route::namespace('Location')->group(function () {
    Route::prefix('location')->as('location.')->group(function () {
        Route::get('cities', 'CityController')->name('cities.index');
        Route::get('postcodes', 'PostcodeController')->name('postcodes.index');
        Route::apiResource('departments', 'DepartmentController');
    });
    Route::apiResource('locations', 'LocationController');
});

Route::namespace('Service')->group(function () {
    Route::prefix('services')->as('services.')->group(function () {
        Route::apiResource('categories', 'CategoryController', ['except' => ['show']]);
    });
    Route::apiResource('services', 'ServiceController');
});

Route::namespace('User')->group(function () {
    Route::apiResource('clients', 'ClientController', ['parameters' => [
        'clients' => 'user'
    ]]);
    Route::apiResource('clients/{user}/addresses', 'ClientAddressController', ['except' => ['show']]);
    Route::apiResource('providers', 'ProviderController', ['parameters' => [
        'providers' => 'user'
    ]]);
    Route::apiResource('admins', 'AdminController', ['parameters' => [
        'admins' => 'user'
    ]]);
});

Route::namespace('News')->group(function () {
    Route::prefix('news')->as('news.')->group(function () {
        Route::apiResource('categories', 'CategoryController', ['except' => ['show']]);
        Route::apiResource('tags', 'TagController', ['except' => ['show']]);
    });
    Route::apiResource('news', 'NewsController');
});

Route::namespace('Question')->group(function () {
    Route::prefix('questions')->as('questions.')->group(function () {
        Route::apiResource('categories', 'CategoryController', ['except' => ['show']]);
    });
    Route::apiResource('questions', 'QuestionController', ['except' => ['show']]);
});

Route::apiResource('workshops', 'WorkshopController', ['only' => ['index', 'destroy']]);
Route::apiResource('feedbacks', 'FeedbackController', ['only' => ['index', 'destroy']]);
Route::apiResource('conditions', 'ConditionController', ['only' => ['show', 'update']]);
Route::apiResource('promocodes', 'PromoCodeController', ['except' => ['show']]);
Route::apiResource('holidays', 'HolidayController', ['except' => ['show']]);

Route::apiResource('orders', 'OrderController');